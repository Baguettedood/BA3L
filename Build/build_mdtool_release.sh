#! /bin/sh
# Builds BA3L with MonoDevelop's "mdtool" build system in Release configuration
# and copies the built files into a folder structure for later packaging.
# Requires MonoDevelop

cd "$(dirname "$0")"
CONFIG="Release"

nuget restore ../

# Build the solution
mdtool build -c:"$CONFIG|x86" ../BA3L.sln

# Copy the solution into our own folder
mkdir -p $CONFIG

# BA3LLauncher
cp ../BA3LLauncher/bin/$CONFIG/arma3launcher.exe $CONFIG/arma3launcher.exe

# BA3L
mkdir -p $CONFIG/BA3L
cp ../BA3L/bin/$CONFIG/BA3L.exe $CONFIG/BA3L/BA3L.exe
cp ../BA3L/bin/$CONFIG/Facepunch.Steamworks.dll $CONFIG/BA3L/Facepunch.Steamworks.dll
cp ../BA3L/bin/$CONFIG/Facepunch.Steamworks.xml $CONFIG/BA3L/Facepunch.Steamworks.xml
cp ../BA3L/bin/$CONFIG/libsteam_api.so $CONFIG/BA3L/libsteam_api.so
cp ../BA3L/bin/$CONFIG/Newtonsoft.Json.dll $CONFIG/BA3L/Newtonsoft.Json.dll
cp ../BA3L/bin/$CONFIG/Newtonsoft.Json.xml $CONFIG/BA3L/Newtonsoft.Json.xml
cp ../BA3L/bin/$CONFIG/steam_api64.dll $CONFIG/BA3L/steam_api64.dll
cp ../BA3L/bin/$CONFIG/Gameloop.Vdf.dll  $CONFIG/BA3L/Gameloop.Vdf.dll
cp ../BA3L/bin/$CONFIG/Gameloop.Vdf.xml $CONFIG/BA3L/Gameloop.Vdf.xml
