﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;

namespace BA3L
{
	public class ConsoleUI
	{
		private readonly Core core;

		public ConsoleUI(Core core)
		{
			this.core = core;
		}

		private enum Command
		{
			CMD_NONE,
			CMD_PLAY,
			CMD_EXIT,
			CMD_PARAMETER,
			CMD_LISTINSTALLEDMODS,
			CMD_TESTING,
			CMD_ENABLELOCALMOD,
			CMD_ENABLEWORKSHOPMOD,
			CMD_DISABLELOCALMOD,
			CMD_DISABLEWORKSHOPMOD,
			CMD_LISTACTIVEMODS,
			CMD_SHOWMODPARAMETERS,
			CMD_LOADPRESET,
			CMD_SAVEPRESET,
			CMD_SHOWEXTRAPARAMS,
			CMD_SETEXTRAPARAMS,
			CMD_ENABLEDLC,
			CMD_DISABLEDLC,
			CMD_SERVER,
		}

		public bool Run()
		{
			Console.WriteLine("Console UI Started");
			Console.WriteLine("BA3L's Console UI is unfinished and intended for development only. Please don't actually use it.");
			if(!EnsureConsoleWindow())
			{
				return false;
			}

			//Loop and input commands, executing until exit
			bool exit = false;
			while(!exit)
			{
				//Get command and interpret it
				string input = "";
				Command c = GetCommand(out input);
				switch(c)
				{
					case Command.CMD_PLAY:
						exit = Play(input);
						break;
					case Command.CMD_EXIT:
						exit = true;
						break;
					case Command.CMD_PARAMETER:
						SetParameter(input);
						break;
					case Command.CMD_LISTINSTALLEDMODS:
						ListInstalledMods();
						break;
					case Command.CMD_TESTING: // Simple testing commands handled in GetCommand()
						break;
					case Command.CMD_ENABLELOCALMOD:
						EnableLocalMod(input);
						break;
					case Command.CMD_DISABLELOCALMOD:
						DisableLocalMod(input);
						break;
					case Command.CMD_ENABLEWORKSHOPMOD:
						EnableWorkshopMod(input);
						break;
					case Command.CMD_DISABLEWORKSHOPMOD:
						DisableWorkshopMod(input);
						break;
					case Command.CMD_LISTACTIVEMODS:
						ListActiveMods();
						break;
					case Command.CMD_SHOWMODPARAMETERS:
						ShowModParameters();
						break;
					case Command.CMD_LOADPRESET:
						LoadPreset();
						break;
					case Command.CMD_SAVEPRESET:
						SavePreset();
						break;
					case Command.CMD_SHOWEXTRAPARAMS:
						ShowExtraParams();
						break;
					case Command.CMD_SETEXTRAPARAMS:
						SetExtraParams();
						break;
					case Command.CMD_ENABLEDLC:
						EnableDlc();
						break;
					case Command.CMD_DISABLEDLC:
						DisableDlc();
						break;
					case Command.CMD_SERVER:
						ServerCommand(input);
						break;
					default:
						Console.WriteLine("Command not recognized.");
						break;
				}
			}

			return true;
		}

		private Command GetCommand(out string input)
		{
			Console.Write("Enter command: ");
			input = Console.ReadLine();
			string inputLower = input.ToLower();

			//Parse command
			if(input.Length > 0)
			{
				if(inputLower.StartsWith("play", StringComparison.Ordinal))
				{
					return Command.CMD_PLAY;
				}
				else if(inputLower == "exit")
				{
					return Command.CMD_EXIT;
				}
				else if(inputLower.StartsWith("parameter", StringComparison.Ordinal))
				{
					return Command.CMD_PARAMETER;
				}
				else if(inputLower == "list installed mods" || inputLower == "lim")
				{
					return Command.CMD_LISTINSTALLEDMODS;
				}
				else if(inputLower == "isproton")
				{
					Console.WriteLine("Using Proton: " + Utilities.IsProton());
					return Command.CMD_TESTING;
				}
				else if(inputLower.StartsWith("enable local mod", StringComparison.Ordinal))
				{
					return Command.CMD_ENABLELOCALMOD;
				}
				else if(inputLower.StartsWith("disable local mod", StringComparison.Ordinal))
				{
					return Command.CMD_DISABLELOCALMOD;
				}
				else if(inputLower.StartsWith("enable workshop mod", StringComparison.Ordinal))
				{
					return Command.CMD_ENABLEWORKSHOPMOD;
				}
				else if(inputLower.StartsWith("disable workshop mod", StringComparison.Ordinal))
				{
					return Command.CMD_DISABLEWORKSHOPMOD;
				}
				else if(inputLower == "list active mods" || inputLower == "list enabled mods")
				{
					return Command.CMD_LISTACTIVEMODS;
				}
				else if(inputLower == "show mod parameters")
				{
					return Command.CMD_SHOWMODPARAMETERS;
				}
				else if(inputLower == "loadpreset")
				{
					return Command.CMD_LOADPRESET;
				}
				else if(inputLower == "savepreset")
				{
					return Command.CMD_SAVEPRESET;
				}
				else if(inputLower == "show extra params")
				{
					return Command.CMD_SHOWEXTRAPARAMS;
				}
				else if(inputLower == "set extra params")
				{
					return Command.CMD_SHOWEXTRAPARAMS;
				}
				else if(inputLower == "enable dlc")
				{
					return Command.CMD_ENABLEDLC;
				}
				else if(inputLower == "disable dlc")
				{
					return Command.CMD_DISABLEDLC;
				}
				else if(inputLower.StartsWith("server", StringComparison.Ordinal))
				{
					return Command.CMD_SERVER;
				}

			}

			// If we don't find a matching command
			return Command.CMD_NONE;
		}


		private bool EnsureConsoleWindow()
		{
			if(Utilities.GetOS() == Utilities.OS.OS_WINDOWS)
			{
				if(GetConsoleWindow() != IntPtr.Zero)
				{
					return true;
				}
				else
				{
					Console.WriteLine("Does not have console");
					return false;
				}
			}

			//Idk how to tell on OSX/Linux so guess true
			return true;
		}

		[DllImport("kernel32.dll")]
		static extern IntPtr GetConsoleWindow();

		private bool Play(string input)
		{
			try
			{
				bool result = core.Play();

				if(result)
				{
					Console.WriteLine("Arma 3 started.");
				}
				else
				{
					Console.WriteLine("Arma 3 not successfully started.");
				}

				return input.EndsWith("exit", StringComparison.Ordinal);
			}
			catch(Exceptions.ModNotReadyException e)
			{
				Console.WriteLine("One or more mods were not ready to launch.");
				if(e.FailedMod != null)
				{
					Console.WriteLine("First failed mod: " + e.FailedMod.Name);
				}

				return false;
			}
			catch(Exceptions.ProtonNotFoundException e)
			{
				Console.WriteLine("PlayProton error: " + e.Message);
				return false;
			}
		}

		private void SetParameter(string input)
		{
			if (input.Length < 0)
				return;

			string[] splitInput = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			if(splitInput.Length <= 1)
			{
				Console.WriteLine("Error: No arguments given.");
				Console.WriteLine("Usage: paramater paramname [value]");
				return;
			}

			if (splitInput.Length >= 2)
			{

				if (splitInput.Length == 2)
				{
					// e.g. parameter paramname
					// Just display the value of the parameter
					string parameterToCheck = splitInput[1].ToLower();
					if(parameterToCheck == Settings.KEY_NOSPLASH.ToLower())
					{
						Console.WriteLine(Settings.KEY_NOSPLASH + " is " + Settings.NoSplash);
					}
					else if(parameterToCheck == Settings.KEY_SKIPINTRO.ToLower())
					{
						Console.WriteLine(Settings.KEY_SKIPINTRO + " is " + Settings.SkipIntro);
					}
					else if(parameterToCheck == Settings.KEY_WINDOWED.ToLower())
					{
						Console.WriteLine(Settings.KEY_WINDOWED + " is " + Settings.Windowed);
					}

				}
				else if (splitInput.Length >= 3)
				{
					// e.g paramater paramname value
					// Change its value
					string parameterToSet = splitInput[1].ToLower();
					try
					{
						if(parameterToSet == Settings.KEY_NOSPLASH.ToLower())
						{
							Settings.NoSplash = StrToBool(splitInput[2]);
						}
						else if(parameterToSet == Settings.KEY_SKIPINTRO.ToLower())
						{
							Settings.SkipIntro = StrToBool(splitInput[2]);
						}
						else if(parameterToSet == Settings.KEY_WINDOWED.ToLower())
						{
							Settings.SkipIntro = StrToBool(splitInput[2]);
						}
					}
					catch(ArgumentException e)
					{
						Console.WriteLine(e.Message);
					}
				}
			}
		}

		private bool StrToBool(string input)
		{
			string inputLower = input.ToLower();
			if(inputLower == "true")
			{
				return true;
			}
			else if(inputLower == "false")
			{
				return false;
			}
			else
			{
				throw new ArgumentException("Error: " + input + " does not resolve to a boolean!");
			}
		}

		private void ListInstalledMods()
		{
			var mods = ModManager.GetMods();

			foreach(Mod mod in mods)
			{
				// Reasonable defaults for the string formatting.
				Console.WriteLine(" Name: {0, -55} | Identifier: {1, -25} | Status: {2}", 
					mod.Name, mod.Identifier, mod.Status);
			}
		}

		private void EnableLocalMod(string input)
		{
			// Trim to find identifier
			// Send to mod manager
			int index = "enable local mod".Length;
			string modIdentifier = input.Substring(index).TrimStart(' ');
			Console.WriteLine("Enabling mod: " + modIdentifier);
			bool result = ModManager.EnableModByFolderName(modIdentifier);
			Console.WriteLine("Mod enabled: " + result);
		}

		private void DisableLocalMod(string input)
		{
			int index = "disable local mod".Length;
			string modIdentifier = input.Substring(index).TrimStart(' ');
			Console.WriteLine("Disabling mod: " + modIdentifier);
			ModManager.DisableModByFolderName(modIdentifier);
		}

		private void EnableWorkshopMod(string input)
		{
			int index = "enable workshop mod".Length;
			string modIdentifierString = input.Substring(index).TrimStart(' ');
			ulong modIdentifier;
			bool success = UInt64.TryParse(modIdentifierString, out modIdentifier);
			if(success)
			{
				Console.WriteLine("Enabling workshop mod: " + modIdentifierString);
				ModManager.EnableModByWorkshopId(modIdentifier);
			}
			else
			{
				Console.WriteLine("Failed to parse mod identifier: " + modIdentifierString);
			}
		}

		private void DisableWorkshopMod(string input)
		{
			int index = "disable workshop mod".Length;
			string modIdentifierString = input.Substring(index).TrimStart(' ');
			ulong modIdentifier;
			bool success = UInt64.TryParse(modIdentifierString, out modIdentifier);
			if(success)
			{
				Console.WriteLine("Disabling workshop mod: " + modIdentifierString);
				ModManager.DisableModByWorkshopId(modIdentifier);
			}
			else
			{
				Console.WriteLine("Failed to parse mod identifier: " + modIdentifierString);
			}
		}

		private void ListActiveMods()
		{
			var activeMods = ModManager.GetEnabledMods();

			foreach(Mod mod in activeMods)
			{
				// Reasonable defaults for the string formatting.
				Console.WriteLine("| Name: {0, -55} | Identifier: {1, -25} | Ready: {2, -6} |",
					mod.Name, mod.Identifier, mod.IsReadyToPlay.ToString());
			}
		}

		private void ShowModParameters()
		{
			try
			{
				Console.WriteLine(ModManager.GenerateModLaunchParameters());
			}
			catch(Exceptions.ModNotReadyException e)
			{
				Console.WriteLine("One or more mods were not ready to generate launch parameters.");
				if(e.FailedMod != null)
				{
					Console.WriteLine("First failed mod: " + e.FailedMod.Name);
				}
			}
		}

		private void SavePreset()
		{
			Console.Write("Enter save name: ");
			string saveName = Console.ReadLine();
			string savePath = System.IO.Path.Combine(Settings.GetLauncherSaveDirectory(), saveName);
			Console.WriteLine("Saving to path: " + @savePath);
			Settings.SaveModPresetFile(savePath, ModManager.GetEnabledMods());
		}

		private void LoadPreset()
		{
			Console.Write("Enter load name: ");
			string loadName = Console.ReadLine();
			if(!loadName.EndsWith(".ba3lsave", StringComparison.Ordinal))
			{
				loadName += ".ba3lsave";
			}
			string loadPath = System.IO.Path.Combine(Settings.GetLauncherSaveDirectory(), loadName);
			Console.WriteLine("Loading from path: " + @loadPath);

			try
			{
				Settings.LoadModPresetFile(loadPath);
			}
			catch(System.IO.FileNotFoundException e)
			{
				Console.WriteLine(e.Message);
			}

		}

		private void ShowExtraParams()
		{
			Console.WriteLine(Settings.ExtraCmdLineParams);
		}

		private void SetExtraParams()
		{
			Console.Write("Current parameters: ");
			ShowExtraParams();
			Console.Write("Input Extra parameters: ");
			string newParams = Console.ReadLine();
			// Hope it's formatted correctly.
			Settings.ExtraCmdLineParams = newParams;
		}

		private void EnableDlc()
		{
			Console.Write("Enter which DLC to enable (GM, Contact, SOG, CSLA): ");
			string input = Console.ReadLine().ToLower();
			if(input == "gm")
			{
				DlcManager.GmEnabled = true;
				if(DlcManager.GmEnabled)
				{
					Console.WriteLine("GM enabled.");
				}
				else
				{
					Console.WriteLine("Unable to enable GM because it is not installed or you do not own it.");
				}
			}
			else if(input == "contact")
			{
				DlcManager.ContactEnabled = true;
				if(DlcManager.ContactEnabled)
				{
					Console.WriteLine("Contact enabled.");
				}
				else
				{
					Console.WriteLine("Unable to enable Contact because it is not installed or you do not own it.");
				}
			}
            else if(input == "sog")
            {
                DlcManager.SogEnabled = true;
                if(DlcManager.SogEnabled)
                {
                    Console.WriteLine("S.O.G. Enabled.");
                }
                else
                {
                    Console.WriteLine("Unable to enable S.O.G. because it is not installed or you do not own it.");
                }
            }
            else if(input == "csla")
            {
                DlcManager.CslaEnabled = true;
                if(DlcManager.CslaEnabled)
                {
                    Console.WriteLine("CSLA Enabled.");
                }
                else
                {
                    Console.WriteLine("Unable to enable CSLA because it is not installed or you do not own it.");
                }
            }
            else
			{
				Console.WriteLine("Unknown DLC: " + input);
			}
		}

		private void DisableDlc()
		{
			Console.Write("Enter which DLC to disable (GM, Contact, SOG, CSLA): ");
			string input = Console.ReadLine().ToLower();
			if(input == "gm")
			{
				DlcManager.GmEnabled = false;
			}
			else if(input == "contact")
			{
				DlcManager.ContactEnabled = false;
			}
            else if (input == "sog")
            {
                DlcManager.SogEnabled = false;
            }
            else if (input == "csla")
            {
                DlcManager.CslaEnabled = false;
            }
            else
			{
				Console.WriteLine("Unknown DLC: " + input);
			}
		}

		private void ServerCommand(string input)
		{
			string inputLower = input.ToLower();

			if(inputLower.Contains("server refresh"))
			{
				ServerBrowser.RefreshInternetServers();
				Console.WriteLine("Refreshing internet servers...");
			}
			else if(inputLower.Contains("server list"))
			{
				ServerList();
			}
			else if(inputLower.Contains("server rules"))
			{
				ServerRules(inputLower);
			}
			else if(inputLower.Contains("server join"))
			{
				ServerJoin(inputLower);
			}
		}

		private void ServerJoin(string inputLower)
		{
			string serverToGet = inputLower.Substring("server join".Length).TrimStart(' ');
			bool success = int.TryParse(serverToGet, out int index);
			if(success)
			{
				var servers = ServerBrowser.GetInternetServerList();
				if(servers.Count == 0)
				{
					Console.WriteLine("Unable to retrieve server list. Refresh servers first.");
					return;
				}
				if(index > servers.Count - 1)
				{
					Console.WriteLine($"Unable to retrieve server index {index}. Highest available index: {servers.Count - 1}");
					Console.WriteLine("Wait for more servers to refresh or choose another server.");
					return;
				}

				var server = servers[index];

				// Display what actions we should take for each mod
				Console.WriteLine($"Showing details for server: {server.Address}:{server.ConnectionPort} - {server.Name}");
				StringBuilder sb = new StringBuilder();
				sb.Append("BattlEye: ");
				if(server.Tags.BattlEye)
					sb.Append("Protected\n");
				else
					sb.Append("Not protected\n");

				sb.Append("Password: ");
				if(server.Passworded)
					sb.Append("Has password\n");
				else
					sb.Append("No password\n");

				sb.Append("Map: ");
				sb.Append(server.Map + "\n"); // TODO still need to check if it shows as Enoch or Livonia. Maybe convert name.
				sb.Append("Mission: ");
				sb.Append(server.Mission + "\n");
				sb.Append($"Players: {server.Players}/{server.MaxPlayers}\n");
				sb.Append($"Server state: {server.Tags.State.ToString()}\n");
				sb.Append($"Difficulty: {server.Tags.Difficulty.ToString()}\n"); // TODO parse difficulty flags

				sb.Append("Server type: ");
				if(server.Tags.Dedicated)
				{
					sb.Append("Dedicated, ");
				}
				else
				{
					sb.Append("Listen, ");
				}

				if(server.Tags.Platform == Utilities.OS.OS_WINDOWS)
				{
					sb.Append("Windows\n");
				}
				else
				{
					sb.Append("Linux\n");
				}

				sb.Append($"Ping: {server.Ping}\n");
				sb.Append($"Required version: {server.Tags.RequiredVersion}\n");

				Console.WriteLine(sb);

				if(server.RulesQueryStatus == RulesQueryInfo.Success)
				{
					List<MatchedMod> matchedMods = ModMatchManager.MatchServerMods(server.Rules.ServerMods, server.Rules.Signatures);

					for(int i = 0; i < matchedMods.Count; i++)
					{
						MatchedMod matched = matchedMods[i];

						if(matched.MatchType == ModMatchType.Modlist)
						{
							// Matched to our mods by the server's mod list
							Console.WriteLine($"{i}: Server mod: {matched.ServerMod.Name} | Current Action: {matched.CurrentAction.ToString()}");
						}
						else if(matched.MatchType == ModMatchType.Signature)
						{
							Console.WriteLine($"{i}: Optional mod | Current action: {matched.CurrentAction.ToString()}");
						}
						else if(matched.MatchType == ModMatchType.NotAllowed)
						{
							Console.WriteLine($"{i} Not allowed | Current action: {matched.CurrentAction.ToString()}");
						}
					}


					Console.WriteLine("Available actions:");
					Console.WriteLine("\taction edit #"); // TODO
					Console.WriteLine("\tapply and join");
					Console.WriteLine("\tjoin (Without changes to mods)");
					Console.WriteLine("\tback");

					string joinInput = Console.ReadLine().ToLower();

					if(joinInput == "apply and join")
					{
						core.ApplyModChangesAndJoin(server, "", matchedMods);
						return;
					}
					else if(joinInput == "join")
					{
						// Don't make changes, and join the server
						core.JoinServer(server);
						// TODO password prompt
						return;
					}
					else if(joinInput == "back")
					{
						return;
					}
					else if(joinInput.StartsWith("action edit", StringComparison.Ordinal))
					{
						// TODO edit actions
					}


				}
				else
				{
					Console.WriteLine($"Rules query status is: {server.RulesQueryStatus.ToString()}");
				}

			}
		}

		private void ServerRules(string inputLower)
		{
			string rulesToGet = inputLower.Substring("server rules".Length).TrimStart(' ');
			bool success = int.TryParse(rulesToGet, out int index);
			if(success)
			{
				var servers = ServerBrowser.GetInternetServerList();
				if(servers.Count == 0)
				{
					Console.WriteLine("Unable to retrieve server list. Refresh servers first.");
					return;
				}
				if(index > servers.Count - 1)
				{
					Console.WriteLine($"Unable to retrieve server index {index}. Highest available index: {servers.Count - 1}");
					Console.WriteLine("Wait for more servers to refresh or choose another server.");
					return;
				}

				var server = servers[index];
				Console.WriteLine($"Displaying rules for server {server.Address}:{server.ConnectionPort} - {server.Name}");
				Console.WriteLine($"Ping: {server.Ping,7} | Mission: {server.Mission}");
				Console.WriteLine($"Version: {server.Version} | Build string: {server.FullVersion}");
				Console.WriteLine($"Players: {server.Players}/{server.MaxPlayers}");
				Console.WriteLine($"BattlEye: {server.Tags.BattlEye}");
				if(server.RulesQueryStatus == RulesQueryInfo.Success)
				{
					Console.WriteLine($"Expansions: {server.Rules.Expansions.ToString()}");
					// Server mods
					Console.WriteLine($"Server mods: {server.Rules.ServerMods.Count}");
					foreach(ServerMod serverMod in server.Rules.ServerMods)
					{
						Console.WriteLine($"\t{serverMod.Name}");
					}

					Console.WriteLine($"Signatures: {server.Rules.Signatures.Count}");
					foreach(string signature in server.Rules.Signatures)
					{
						Console.WriteLine($"\t{signature}");
					}
				}
				else
				{
					Console.WriteLine($"Unable to get rules because rules query status is: {server.RulesQueryStatus.ToString()}");
				}

			}
			else
			{
				Console.WriteLine("Unknown server index.");
			}
		}

		private static void ServerList()
		{
			var servers = ServerBrowser.GetInternetServerList();

			int idx = 0;
			foreach(var server in servers)
			{
				Console.WriteLine($"{idx}: [{server.Address}:{server.ConnectionPort}]\t{server.Name}");
				idx++;
			}

			Console.WriteLine($"Listed {servers.Count} servers...");
		}
	}
}