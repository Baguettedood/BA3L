﻿using System;
using System.IO;
using Newtonsoft.Json; // https://www.newtonsoft.com/json
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace BA3L
{

	public enum GameStartAction
	{
		DO_NOTHING,
		MINIMIZE,
		CLOSE,
		CLOSE_AFTER_GAME_ENDS
	};

	/// <summary>
	/// Settings.cs
	/// Stores configuration data for Arma and the Launcher
	/// Saves and loads settings from a JSON file.
	/// </summary>
	public static class Settings
	{
		// BA3L Settings / Launcher Options
		// Language?
		// Minimize Launcher to tray? (GUI only if that's even possible. Might ignore.)
		// Action after game start - Do Nothing, Minimize, Close, Close after game ends
		public static GameStartAction ActionAfterGameStart = GameStartAction.DO_NOTHING;
		// Default launcher page? (Would be GUI only)
		// Enable analytics (Seems to be some kind of setting for Arma 3 itself)
		// Watched mod folders (Default is Arma 3 folder. Workshop is handled separately.)
		
		// Saved active mods
		public static ReadOnlyCollection<string> SavedLocalMods;
		public static ReadOnlyCollection<ulong> SavedWorkshopMods;

		// Arma 3 Settings/Parameters
		// Basic:

		public static bool NoSplash = false;   // "Skip logos at startup"
		public static bool SkipIntro = false;  // "Show static menu background"
		public static bool Windowed = false;   // "Force start in a window"
		public static string Profile = "";     // "Profile Name" - Official launcher has a list of profiles
											   // Preselected unit?
		public static string MissionFile = ""; // Mission file (Editor)
		public static string ExtraCmdLineParams = "";

		public static bool PlayWithBattlEye = false;


		// Advanced: https://community.bistudio.com/wiki/Arma_3_Launcher_-_Advanced_Parameters
		// TODO more parameters

		// Json keys
		public const string KEY_NOSPLASH = "NoSplash";
		public const string KEY_SKIPINTRO = "SkipIntro";
		public const string KEY_WINDOWED = "Windowed";
		public const string KEY_ENABLEDMODSLOCAL = "EnabledModsLocal";
		public const string KEY_ENABLEDMODSWORKSHOP = "EnabledModsWorkshop";
		public const string KEY_PLAYWITHBATTLEYE = "PlayWithBattlEye";
		public const string KEY_EXTRACMDLINEPARAMS = "ExtraCmdLineParams";
		public const string KEY_GMENABLED = "GmEnabled";
		public const string KEY_CONTACTENABLED = "ContactEnabled";
        public const string KEY_SOGENABLED = "SogEnabled";
        public const string KEY_CSLAENABLED = "CslaEnabled";
        public const string KEY_WSAHARAENABLED = "WSaharaEnabled";

        // File extensions for mod presets
        public const string EXT_BA3LSAVE = ".ba3lsave"; // BA3L's mod preset format
		public const string EXT_HTML = ".html";         // Official launcher's HTML format
		public const string EXT_A3ULML = ".a3ulml";     // Muttley's Arma 3 Unix Launcher format

		/// <summary>
		/// Saves the launcher's current settings to a specified path.
		/// Saves parameters and mods.
		/// </summary>
		/// <returns>Whether or not the save was successful.</returns>
		/// <param name="path">File path.</param>
		/// <param name="enabledMods">Enabled mods.</param>
		public static bool Save(string path, ReadOnlyCollection<Mod> enabledMods)
		{
			JObject json = new JObject();

			// Parameters
			json[KEY_NOSPLASH] = NoSplash;
			json[KEY_SKIPINTRO] = SkipIntro;
			json[KEY_WINDOWED] = Windowed;

			if(ExtraCmdLineParams != "")
			{
				json[KEY_EXTRACMDLINEPARAMS] = ExtraCmdLineParams;
			}

			// BA3L Options
			json[KEY_PLAYWITHBATTLEYE] = PlayWithBattlEye;

			// DLCs
			json[KEY_GMENABLED] = DlcManager.GmEnabled;
			json[KEY_CONTACTENABLED] = DlcManager.ContactEnabled;
            json[KEY_SOGENABLED] = DlcManager.SogEnabled;
            json[KEY_CSLAENABLED] = DlcManager.CslaEnabled;
            json[KEY_WSAHARAENABLED] = DlcManager.WSaharaEnabled;

			// Mods
			if(enabledMods != null && enabledMods.Any())
			{
				JArray jsonEnabledModsLocal = new JArray();
				JArray jsonEnabledModsWorkshop = new JArray();

				foreach(Mod mod in enabledMods)
				{
					if(mod.UsesSteamWorkshop())
					{
						jsonEnabledModsWorkshop.Add(mod.WorkshopId);
					}
					else
					{
						jsonEnabledModsLocal.Add(mod.GetPath());
					}
				}

				json[KEY_ENABLEDMODSLOCAL] = jsonEnabledModsLocal;
				json[KEY_ENABLEDMODSWORKSHOP] = jsonEnabledModsWorkshop;
			}

			//More things to save


			Directory.CreateDirectory(GetLauncherSaveDirectory());
			StreamWriter writer = new StreamWriter(path, false); // Override old file if it exists
			JsonTextWriter jsonWriter = new JsonTextWriter(writer);
			json.WriteTo(jsonWriter);
			jsonWriter.Close();

			// TODO deal with possible exceptions
			return true;
		}

		/// <summary>
		/// Loads the launcher's previously saved settings from a specified path.
		/// </summary>
		/// <returns>Whether or not the load was successful.</returns>
		/// <param name="path">File path.</param>
		public static bool Load(string path)
		{
			if(!File.Exists(path))
			{
				return false;
			}

			StreamReader reader = File.OpenText(path);
			JObject json = JObject.Parse(reader.ReadToEnd());

			// BA3L Options
			PlayWithBattlEye = LoadBoolValue(json, KEY_PLAYWITHBATTLEYE, false);
			ExtraCmdLineParams = LoadStringValue(json, KEY_EXTRACMDLINEPARAMS, "");

			// DLCs
			DlcManager.GmEnabled = LoadBoolValue(json, KEY_GMENABLED, false);
			DlcManager.ContactEnabled = LoadBoolValue(json, KEY_CONTACTENABLED, false);
            DlcManager.SogEnabled = LoadBoolValue(json, KEY_SOGENABLED, false);
            DlcManager.CslaEnabled = LoadBoolValue(json, KEY_CSLAENABLED, false);
            DlcManager.WSaharaEnabled = LoadBoolValue(json, KEY_WSAHARAENABLED, false);

			// Load active mod list
			JArray jsonEnabledModsLocal = (JArray)json[KEY_ENABLEDMODSLOCAL];
			JArray jsonEnabledModsWorkshop = (JArray)json[KEY_ENABLEDMODSWORKSHOP];

			List<string> enabledModsLocal = new List<string>();
			List<ulong> enabledModsWorkshop = new List<ulong>();

			if(jsonEnabledModsLocal != null)
			{
				foreach(var entry in jsonEnabledModsLocal)
				{
					enabledModsLocal.Add((string)entry);
				}
			}

			if(jsonEnabledModsWorkshop != null)
			{
				foreach(var entry in jsonEnabledModsWorkshop)
				{
					enabledModsWorkshop.Add((ulong)entry);
				}
			}

			SavedLocalMods = new ReadOnlyCollection<string>(enabledModsLocal);
			SavedWorkshopMods = new ReadOnlyCollection<ulong>(enabledModsWorkshop);



			// Arma 3 options
			NoSplash = LoadBoolValue(json, KEY_NOSPLASH, false);
			SkipIntro = LoadBoolValue(json, KEY_SKIPINTRO, false);
			Windowed = LoadBoolValue(json, KEY_WINDOWED, false);

			reader.Close();
			// TODO deal with possible exceptions
			return true;
		}

		private static string LoadStringValue(JObject json, string valueToGet, string defaultValue = "")
		{
			if(json.TryGetValue(valueToGet, out JToken tempOutput))
			{
				return (string)tempOutput;
			}
			else
			{
				return defaultValue;
			}
		}

		private static bool LoadBoolValue(JObject json, string valueToGet, bool defaultValue)
		{
			if(json.TryGetValue(valueToGet, out JToken tempOutput))
			{
				return (bool)tempOutput;
			}
			else
			{
				return defaultValue;
			}
		}

		public static string GetLauncherSaveDirectory()
		{
			return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "BA3L");
		}

		public static string GetLauncherSavePath()
		{
			return Path.Combine(GetLauncherSaveDirectory(), "settings.json");
		}

		/// <summary>
		/// Saves the currently-enabled mods to a preset file.
		/// </summary>
		/// <param name="path">Path to the new preset</param>
		/// <param name="enabledMods">Enabled mods</param>
		public static void SaveModPresetFile(string path, ReadOnlyCollection<Mod> enabledMods)
		{
			if(enabledMods == null)
				return;

			// Force save files to end with ".ba3lsave" in case the UI doesn't auto-add this.
			if(!path.EndsWith(EXT_BA3LSAVE, StringComparison.Ordinal))
			{
				path = path + EXT_BA3LSAVE;
			}

			// Unlike Save, do continue if enabledMods has no entries since "no mods" is a valid preset.
			// That being said, we will likely also eventually have a "launch without mods" button.
			JObject json = new JObject();
			JArray jsonEnabledModsLocal = new JArray();
			JArray jsonEnabledModsWorkshop = new JArray();

			foreach (Mod mod in enabledMods)
			{
				if (mod.UsesSteamWorkshop())
				{
					jsonEnabledModsWorkshop.Add(mod.WorkshopId);
				}
				else
				{
					// Using Identifier instead of Path
					// Because I don't want to assume this won't be shared.
					jsonEnabledModsLocal.Add(mod.Identifier);
				}
			}

			// Put into JObject
			json[KEY_ENABLEDMODSLOCAL] = jsonEnabledModsLocal;
			json[KEY_ENABLEDMODSWORKSHOP] = jsonEnabledModsWorkshop;

			// Write to file
			Directory.CreateDirectory(GetLauncherSaveDirectory());
			StreamWriter writer = new StreamWriter(path, false); // Override old file if it exists
			JsonTextWriter jsonWriter = new JsonTextWriter(writer);
			json.WriteTo(jsonWriter);
			jsonWriter.Close();
		}

		/// <summary>
		/// Loads a saved mod preset.
		/// </summary>
		/// <returns>The mod preset.</returns>
		/// <param name="path">Path to the preset.</param>
		public static void LoadModPresetFile(string path)
		{
			ModPresetStruct modPreset = LoadModPreset(path);
			ModManager.LoadModPreset(modPreset);
		}

		public static ModPresetStruct LoadModPreset(string path)
		{
			if(!File.Exists(path))
			{
				throw new FileNotFoundException("Unable to find mod preset: " + path);
			}

			if(path.EndsWith(EXT_BA3LSAVE, StringComparison.Ordinal))
			{
				return LoadModPresetBa3lsave(path);
			}
			else if(path.EndsWith(EXT_HTML, StringComparison.Ordinal))
			{
				return LoadModPresetHtml(path);
			}
			else if(path.EndsWith(EXT_A3ULML, StringComparison.Ordinal))
			{
				return LoadModPresetArma3UnixLauncher(path);
			}
			else
			{
				// Assume BA3L save I guess
				// Are there other types of presets to attempt parsing?
				return LoadModPresetBa3lsave(path);
			}

		}

		private static ModPresetStruct LoadModPresetBa3lsave(string path)
		{
			StreamReader reader = File.OpenText(path);
			JObject json = JObject.Parse(reader.ReadToEnd());
			reader.Close();

			JArray jsonEnabledModsLocal = (JArray)json[KEY_ENABLEDMODSLOCAL];
			JArray jsonEnabledModsWorkshop = (JArray)json[KEY_ENABLEDMODSWORKSHOP];

			List<string> enabledModsLocal = new List<string>();
			List<ulong> enabledModsWorkshop = new List<ulong>();

			if(jsonEnabledModsLocal != null)
			{
				foreach(var entry in jsonEnabledModsLocal)
				{
					enabledModsLocal.Add((string)entry);
				}
			}

			if(jsonEnabledModsWorkshop != null)
			{
				foreach(var entry in jsonEnabledModsWorkshop)
				{
					enabledModsWorkshop.Add((ulong)entry);
				}
			}

			ModPresetStruct presetMods;
			presetMods.LocalModIdentifiers = enabledModsLocal;
			presetMods.WorkshopModIds = enabledModsWorkshop;
			return presetMods;
		}

		/// <summary>
		/// Loads an HTML mod preset created by the official Arma 3 Launcher.
		/// </summary>
		/// <param name="path">Path.</param>
		public static ModPresetStruct LoadModPresetHtml(string path)
		{
			// TODO Figure out if this is only for Steam mods or local mods too
			// Is it also for DLCs?

			StreamReader reader = new StreamReader(path);
			string contents = reader.ReadToEnd();
			reader.Close();
			char[] separators = { '\r', '\n' };
			string[] lines = contents.Split(separators, StringSplitOptions.RemoveEmptyEntries);

			// Look for something formatted like a Steam Workshop URL
			// Example: https://steamcommunity.com/sharedfiles/filedetails/?id=450814997
			// or       http://steamcommunity.com/sharedfiles/filedetails/?id=861133494
			// So pretty much  steamcommunity.com/sharedfiles/filedetails/?id=modId
			Regex workshopModRegex = new Regex(@"steamcommunity\.com/sharedfiles/filedetails/\?id=(?<id>(\d+))");
			List<ulong> workshopModIds = new List<ulong>();

			foreach(string line in lines)
			{
				// Match one of the regexes and do stuff
				Match keyValueMatch = workshopModRegex.Match(line);
				if(!keyValueMatch.Success)
					continue;

				string modIdString = keyValueMatch.Groups["id"].Value;
				if(ulong.TryParse(modIdString, out ulong modId))
				{
					workshopModIds.Add(modId);
				}
			}

			ModPresetStruct modpreset;
			modpreset.LocalModIdentifiers = new List<string>();
			modpreset.WorkshopModIds = workshopModIds;

			return modpreset;
		}

		public static ModPresetStruct LoadModPresetArma3UnixLauncher(string path)
		{
			StreamReader reader = File.OpenText(path);
			JObject json = JObject.Parse(reader.ReadToEnd());
			reader.Close();

			List<string> enabledModsLocal = new List<string>();
			List<ulong> enabledModsWorkshop = new List<ulong>();

			JToken jsonMods = json["mods"];
			if(jsonMods == null)
			{
				// Communicate an error that this isn't a valid preset
				// Return empty mod list for now
				ModPresetStruct presetModsNone;
				presetModsNone.LocalModIdentifiers = enabledModsLocal;
				presetModsNone.WorkshopModIds = enabledModsWorkshop;
				return presetModsNone;
			}

			//JArray jsonEnabledModsLocal = (JArray)jsonMods["custom"]; // TODO figure out structure
			JArray jsonEnabledModsWorkshop = (JArray)jsonMods["workshop"];


			if(jsonEnabledModsWorkshop != null)
			{
				foreach(var entry in jsonEnabledModsWorkshop)
				{
					var token = (string)entry.SelectToken("id");
					enabledModsWorkshop.Add(ulong.Parse(token));
				}
			}
			
			ModPresetStruct presetMods;
			presetMods.LocalModIdentifiers = enabledModsLocal;
			presetMods.WorkshopModIds = enabledModsWorkshop;
			return presetMods;
		}
	}
}
