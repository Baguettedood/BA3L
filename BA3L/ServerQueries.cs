﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using Steamworks.Data;

namespace BA3L
{
	public static class ServerQueries
	{


		public static async Task<Dictionary<byte[], byte[]>> GetRules(ServerInfo server)
		{
			var endpoint = new IPEndPoint(server.Address, server.QueryPort);

			using(var client = new UdpClient())
			{
				client.Client.SendTimeout = 3000;
				client.Client.ReceiveTimeout = 3000;
				client.Connect(endpoint);

				return await GetRules(client);
			}
		}

		private static async Task<Dictionary<byte[], byte[]>> GetRules(UdpClient client)
		{
			var challengeBytes = await GetChallengeData(client);
			challengeBytes[0] = A2S_RULES;
			await Send(client, challengeBytes);
			var ruleData = await Receive(client);

			var rules = new Dictionary<byte[], byte[]>();
			using(var br = new BinaryReader(new MemoryStream(ruleData)))
			{
				if(br.ReadByte() != 0x45)
				{
					throw new Exceptions.InvalidRulesException("Invalid response to A2S_RULES request!");
				}

				var numRules = br.ReadUInt16();
				for(int index = 0; index < numRules; index++)
				{
					rules.Add(br.ReadNullTerminatedByteArray(), br.ReadNullTerminatedByteArray());
				}
			}

			return rules;
		}


		// Adapted code from Facepunch.Steamworks' ReadNullTerminatedUTF8String
		// https://github.com/Facepunch/Facepunch.Steamworks/blob/master/Facepunch.Steamworks/Utility/Utility.cs
		public static byte[] ReadNullTerminatedByteArray(this BinaryReader br)
		{
			List<byte> buffer = new List<byte>();

			byte chr;
			int i = 0;
			while((chr = br.ReadByte()) != 0 && i < br.BaseStream.Length)
			{
				buffer.Add(chr);
				i++;
			}

			return buffer.ToArray();
		}

		/// <summary>
		/// Reads a string where its length is declared before the string
		/// This might be the same as BinaryReader.ReadString().
		/// </summary>
		/// <returns>The string.</returns>
		public static string ReadLengthPrefixedString(this BinaryReader br)
		{
			// idk if br.ReadString() does this.
			int stringLength = br.ReadByte();
			byte[] stringBytes = br.ReadBytes(stringLength);
			return Encoding.UTF8.GetString(stringBytes);
		}

		#region FacepunchSteamworksCode
		/*
		 * This code is taken from Facepunch.Steamworks' SourceServerQuery class.
		 * https://github.com/Facepunch/Facepunch.Steamworks/blob/master/Facepunch.Steamworks/Utility/SourceServerQuery.cs	 
		 * Arma 3 uses an A2S_RULES query to store its mod/signature information
		 * But Facepunch.Steamworks' implementation of retrieving this query is not fully suitable for this task
		 * because the payload of Arma 3's "rules" is not UTF-8 as the rules query expects.
		 * We need to retrieve the exact bytes instead.	 
		 */

		private static readonly byte[] A2S_SERVERQUERY_GETCHALLENGE = { 0x55, 0xFF, 0xFF, 0xFF, 0xFF };
		private static readonly byte A2S_RULES = 0x56;

		private static async Task<byte[]> GetChallengeData(UdpClient client)
		{
			await Send(client, A2S_SERVERQUERY_GETCHALLENGE);

			var challengeData = await Receive(client);

			if(challengeData[0] != 0x41)
				throw new Exception("Invalid Challenge");

			return challengeData;
		}

		private static async Task Send(UdpClient client, byte[] message)
		{
			byte[] sendBuffer = new byte[1024];
			sendBuffer[0] = 0xFF;
			sendBuffer[1] = 0xFF;
			sendBuffer[2] = 0xFF;
			sendBuffer[3] = 0xFF;

			Buffer.BlockCopy(message, 0, sendBuffer, 4, message.Length);

			await client.SendAsync(sendBuffer, message.Length + 4);
		}

		private static async Task<byte[]> Receive(UdpClient client)
		{
			byte[][] packets = null;
			byte packetNumber = 0, packetCount = 1;

			do
			{
				var result = await client.ReceiveAsync();
				var buffer = result.Buffer;

				using(var br = new BinaryReader(new MemoryStream(buffer)))
				{
					var header = br.ReadInt32();

					if(header == -1)
					{
						var unsplitdata = new byte[buffer.Length - br.BaseStream.Position];
						Buffer.BlockCopy(buffer, (int)br.BaseStream.Position, unsplitdata, 0, unsplitdata.Length);
						return unsplitdata;
					}
					else if(header == -2)
					{
						int requestId = br.ReadInt32();
						packetNumber = br.ReadByte();
						packetCount = br.ReadByte();
						int splitSize = br.ReadInt32();
					}
					else
					{
						throw new System.Exception("Invalid Header");
					}

					if(packets == null) packets = new byte[packetCount][];

					var data = new byte[buffer.Length - br.BaseStream.Position];
					Buffer.BlockCopy(buffer, (int)br.BaseStream.Position, data, 0, data.Length);
					packets[packetNumber] = data;
				}
			}
			while(packets.Any(p => p == null));

			var combinedData = Combine(packets);
			return combinedData;
		}

		static byte[] Combine(byte[][] arrays)
		{
			var rv = new byte[arrays.Sum(a => a.Length)];
			int offset = 0;
			foreach(byte[] array in arrays)
			{
				Buffer.BlockCopy(array, 0, rv, offset, array.Length);
				offset += array.Length;
			}
			return rv;
		}

		#endregion

	}
}
