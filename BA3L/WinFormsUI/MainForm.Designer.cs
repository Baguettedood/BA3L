using System.ComponentModel;

namespace BA3L.WinFormsUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPlay = new System.Windows.Forms.TabPage();
            this.tlPlay = new System.Windows.Forms.TableLayoutPanel();
            this.btnPlayWithoutMods = new System.Windows.Forms.Button();
            this.btnPlayWithMods = new System.Windows.Forms.Button();
            this.cbPlayBattlEye = new System.Windows.Forms.CheckBox();
            this.tabParameters = new System.Windows.Forms.TabPage();
            this.tlParameters = new System.Windows.Forms.TableLayoutPanel();
            this.cbNoSplash = new System.Windows.Forms.CheckBox();
            this.cbSkipIntro = new System.Windows.Forms.CheckBox();
            this.cbWindowed = new System.Windows.Forms.CheckBox();
            this.cbExtraParams = new System.Windows.Forms.CheckBox();
            this.txtExtraParams = new System.Windows.Forms.TextBox();
            this.tabMods = new System.Windows.Forms.TabPage();
            this.tlMods = new System.Windows.Forms.TableLayoutPanel();
            this.dgvMods = new System.Windows.Forms.DataGridView();
            this.colEnabled = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colModName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIdentifier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tlModSaveLoad = new System.Windows.Forms.TableLayoutPanel();
            this.btnSaveModPreset = new System.Windows.Forms.Button();
            this.btnLoadModPreset = new System.Windows.Forms.Button();
            this.tabServers = new System.Windows.Forms.TabPage();
            this.tabOptions = new System.Windows.Forms.TabPage();
            this.tlOptions = new System.Windows.Forms.TableLayoutPanel();
            this.tlProtonOption = new System.Windows.Forms.TableLayoutPanel();
            this.btnProtonExe = new System.Windows.Forms.Button();
            this.lblProtonExe = new System.Windows.Forms.Label();
            this.cbEnableGm = new System.Windows.Forms.CheckBox();
            this.cbEnableContact = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPlay.SuspendLayout();
            this.tlPlay.SuspendLayout();
            this.tabParameters.SuspendLayout();
            this.tlParameters.SuspendLayout();
            this.tabMods.SuspendLayout();
            this.tlMods.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMods)).BeginInit();
            this.tlModSaveLoad.SuspendLayout();
            this.tabOptions.SuspendLayout();
            this.tlOptions.SuspendLayout();
            this.tlProtonOption.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPlay);
            this.tabControl1.Controls.Add(this.tabParameters);
            this.tabControl1.Controls.Add(this.tabMods);
            this.tabControl1.Controls.Add(this.tabServers);
            this.tabControl1.Controls.Add(this.tabOptions);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(672, 660);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPlay
            // 
            this.tabPlay.Controls.Add(this.tlPlay);
            this.tabPlay.Location = new System.Drawing.Point(4, 22);
            this.tabPlay.Name = "tabPlay";
            this.tabPlay.Padding = new System.Windows.Forms.Padding(3);
            this.tabPlay.Size = new System.Drawing.Size(664, 634);
            this.tabPlay.TabIndex = 3;
            this.tabPlay.Text = "Play";
            this.tabPlay.UseVisualStyleBackColor = true;
            // 
            // tlPlay
            // 
            this.tlPlay.AutoSize = true;
            this.tlPlay.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlPlay.ColumnCount = 1;
            this.tlPlay.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlPlay.Controls.Add(this.btnPlayWithoutMods, 0, 3);
            this.tlPlay.Controls.Add(this.btnPlayWithMods, 0, 4);
            this.tlPlay.Controls.Add(this.cbPlayBattlEye, 0, 0);
            this.tlPlay.Controls.Add(this.cbEnableGm, 0, 1);
            this.tlPlay.Controls.Add(this.cbEnableContact, 0, 2);
            this.tlPlay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlPlay.Location = new System.Drawing.Point(3, 3);
            this.tlPlay.Name = "tlPlay";
            this.tlPlay.RowCount = 6;
            this.tlPlay.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlPlay.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlPlay.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlPlay.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlPlay.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlPlay.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlPlay.Size = new System.Drawing.Size(658, 628);
            this.tlPlay.TabIndex = 3;
            // 
            // btnPlayWithoutMods
            // 
            this.btnPlayWithoutMods.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPlayWithoutMods.Location = new System.Drawing.Point(3, 72);
            this.btnPlayWithoutMods.Name = "btnPlayWithoutMods";
            this.btnPlayWithoutMods.Size = new System.Drawing.Size(652, 23);
            this.btnPlayWithoutMods.TabIndex = 4;
            this.btnPlayWithoutMods.Text = "Play Without Mods";
            this.btnPlayWithoutMods.UseVisualStyleBackColor = true;
            this.btnPlayWithoutMods.Click += new System.EventHandler(this.BtnPlayWithoutMods_Click);
            // 
            // btnPlayWithMods
            // 
            this.btnPlayWithMods.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPlayWithMods.Location = new System.Drawing.Point(3, 101);
            this.btnPlayWithMods.Name = "btnPlayWithMods";
            this.btnPlayWithMods.Size = new System.Drawing.Size(652, 23);
            this.btnPlayWithMods.TabIndex = 6;
            this.btnPlayWithMods.Text = "Play With Mods";
            this.btnPlayWithMods.UseVisualStyleBackColor = true;
            this.btnPlayWithMods.Click += new System.EventHandler(this.BtnPlayWithMods_Click);
            // 
            // cbPlayBattlEye
            // 
            this.cbPlayBattlEye.AutoSize = true;
            this.cbPlayBattlEye.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbPlayBattlEye.Location = new System.Drawing.Point(3, 3);
            this.cbPlayBattlEye.Name = "cbPlayBattlEye";
            this.cbPlayBattlEye.Size = new System.Drawing.Size(652, 17);
            this.cbPlayBattlEye.TabIndex = 7;
            this.cbPlayBattlEye.Text = "Enable BattlEye";
            this.cbPlayBattlEye.UseVisualStyleBackColor = true;
            this.cbPlayBattlEye.CheckedChanged += new System.EventHandler(this.cbPlayBattlEye_CheckedChanged);
            // 
            // tabParameters
            // 
            this.tabParameters.Controls.Add(this.tlParameters);
            this.tabParameters.Location = new System.Drawing.Point(4, 22);
            this.tabParameters.Name = "tabParameters";
            this.tabParameters.Padding = new System.Windows.Forms.Padding(3);
            this.tabParameters.Size = new System.Drawing.Size(664, 634);
            this.tabParameters.TabIndex = 0;
            this.tabParameters.Text = "Parameters";
            this.tabParameters.UseVisualStyleBackColor = true;
            // 
            // tlParameters
            // 
            this.tlParameters.ColumnCount = 2;
            this.tlParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlParameters.Controls.Add(this.cbNoSplash, 0, 0);
            this.tlParameters.Controls.Add(this.cbSkipIntro, 0, 1);
            this.tlParameters.Controls.Add(this.cbWindowed, 0, 2);
            this.tlParameters.Controls.Add(this.cbExtraParams, 0, 3);
            this.tlParameters.Controls.Add(this.txtExtraParams, 1, 3);
            this.tlParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlParameters.Location = new System.Drawing.Point(3, 3);
            this.tlParameters.Name = "tlParameters";
            this.tlParameters.RowCount = 5;
            this.tlParameters.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlParameters.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlParameters.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlParameters.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlParameters.Size = new System.Drawing.Size(658, 628);
            this.tlParameters.TabIndex = 3;
            // 
            // cbNoSplash
            // 
            this.cbNoSplash.Location = new System.Drawing.Point(3, 3);
            this.cbNoSplash.Name = "cbNoSplash";
            this.cbNoSplash.Size = new System.Drawing.Size(180, 21);
            this.cbNoSplash.TabIndex = 1;
            this.cbNoSplash.Text = "Skip logos at startup";
            this.cbNoSplash.UseVisualStyleBackColor = true;
            // 
            // cbSkipIntro
            // 
            this.cbSkipIntro.Location = new System.Drawing.Point(3, 30);
            this.cbSkipIntro.Name = "cbSkipIntro";
            this.cbSkipIntro.Size = new System.Drawing.Size(180, 21);
            this.cbSkipIntro.TabIndex = 2;
            this.cbSkipIntro.Text = "Show static menu background";
            this.cbSkipIntro.UseVisualStyleBackColor = true;
            // 
            // cbWindowed
            // 
            this.cbWindowed.Location = new System.Drawing.Point(3, 57);
            this.cbWindowed.Name = "cbWindowed";
            this.cbWindowed.Size = new System.Drawing.Size(180, 21);
            this.cbWindowed.TabIndex = 3;
            this.cbWindowed.Text = "Force start in a window";
            this.cbWindowed.UseVisualStyleBackColor = true;
            // 
            // cbExtraParams
            // 
            this.cbExtraParams.AutoSize = true;
            this.cbExtraParams.Location = new System.Drawing.Point(3, 84);
            this.cbExtraParams.Name = "cbExtraParams";
            this.cbExtraParams.Size = new System.Drawing.Size(176, 17);
            this.cbExtraParams.TabIndex = 4;
            this.cbExtraParams.Text = "Extra command line parameters:";
            this.cbExtraParams.UseVisualStyleBackColor = true;
            // 
            // txtExtraParams
            // 
            this.txtExtraParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtExtraParams.Location = new System.Drawing.Point(189, 84);
            this.txtExtraParams.Name = "txtExtraParams";
            this.txtExtraParams.Size = new System.Drawing.Size(466, 20);
            this.txtExtraParams.TabIndex = 5;
            this.txtExtraParams.TextChanged += new System.EventHandler(this.TxtExtraParams_TextChanged);
            // 
            // tabMods
            // 
            this.tabMods.Controls.Add(this.tlMods);
            this.tabMods.Location = new System.Drawing.Point(4, 22);
            this.tabMods.Name = "tabMods";
            this.tabMods.Padding = new System.Windows.Forms.Padding(3);
            this.tabMods.Size = new System.Drawing.Size(664, 634);
            this.tabMods.TabIndex = 1;
            this.tabMods.Text = "Mods";
            this.tabMods.UseVisualStyleBackColor = true;
            // 
            // tlMods
            // 
            this.tlMods.ColumnCount = 1;
            this.tlMods.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlMods.Controls.Add(this.dgvMods, 0, 0);
            this.tlMods.Controls.Add(this.tlModSaveLoad, 0, 1);
            this.tlMods.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlMods.Location = new System.Drawing.Point(3, 3);
            this.tlMods.Name = "tlMods";
            this.tlMods.RowCount = 2;
            this.tlMods.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlMods.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlMods.Size = new System.Drawing.Size(658, 628);
            this.tlMods.TabIndex = 4;
            // 
            // dgvMods
            // 
            this.dgvMods.AllowUserToAddRows = false;
            this.dgvMods.AllowUserToDeleteRows = false;
            this.dgvMods.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMods.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colEnabled,
            this.colModName,
            this.colIdentifier,
            this.colStatus});
            this.dgvMods.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMods.Location = new System.Drawing.Point(3, 3);
            this.dgvMods.Name = "dgvMods";
            this.dgvMods.ReadOnly = true;
            this.dgvMods.Size = new System.Drawing.Size(652, 582);
            this.dgvMods.TabIndex = 2;
            // 
            // colEnabled
            // 
            this.colEnabled.DataPropertyName = "Enabled";
            this.colEnabled.FillWeight = 30F;
            this.colEnabled.HeaderText = "Enabled";
            this.colEnabled.Name = "colEnabled";
            this.colEnabled.ReadOnly = true;
            this.colEnabled.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colModName
            // 
            this.colModName.DataPropertyName = "Name";
            this.colModName.FillWeight = 111.9289F;
            this.colModName.HeaderText = "Mod name";
            this.colModName.Name = "colModName";
            this.colModName.ReadOnly = true;
            // 
            // colIdentifier
            // 
            this.colIdentifier.DataPropertyName = "Identifier";
            this.colIdentifier.FillWeight = 111.9289F;
            this.colIdentifier.HeaderText = "Identifier";
            this.colIdentifier.Name = "colIdentifier";
            this.colIdentifier.ReadOnly = true;
            // 
            // colStatus
            // 
            this.colStatus.DataPropertyName = "Status";
            this.colStatus.HeaderText = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.ReadOnly = true;
            // 
            // tlModSaveLoad
            // 
            this.tlModSaveLoad.ColumnCount = 2;
            this.tlModSaveLoad.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlModSaveLoad.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlModSaveLoad.Controls.Add(this.btnSaveModPreset, 0, 0);
            this.tlModSaveLoad.Controls.Add(this.btnLoadModPreset, 1, 0);
            this.tlModSaveLoad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlModSaveLoad.Location = new System.Drawing.Point(3, 591);
            this.tlModSaveLoad.Name = "tlModSaveLoad";
            this.tlModSaveLoad.RowCount = 1;
            this.tlModSaveLoad.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlModSaveLoad.Size = new System.Drawing.Size(652, 34);
            this.tlModSaveLoad.TabIndex = 3;
            // 
            // btnSaveModPreset
            // 
            this.btnSaveModPreset.Location = new System.Drawing.Point(3, 3);
            this.btnSaveModPreset.Name = "btnSaveModPreset";
            this.btnSaveModPreset.Size = new System.Drawing.Size(75, 23);
            this.btnSaveModPreset.TabIndex = 2;
            this.btnSaveModPreset.Text = "Save Preset";
            this.btnSaveModPreset.UseVisualStyleBackColor = true;
            this.btnSaveModPreset.Click += new System.EventHandler(this.BtnSaveModPreset_Click);
            // 
            // btnLoadModPreset
            // 
            this.btnLoadModPreset.Location = new System.Drawing.Point(84, 3);
            this.btnLoadModPreset.Name = "btnLoadModPreset";
            this.btnLoadModPreset.Size = new System.Drawing.Size(75, 23);
            this.btnLoadModPreset.TabIndex = 3;
            this.btnLoadModPreset.Text = "Load Preset";
            this.btnLoadModPreset.UseVisualStyleBackColor = true;
            this.btnLoadModPreset.Click += new System.EventHandler(this.BtnLoadModPreset_Click);
            // 
            // tabServers
            // 
            this.tabServers.Location = new System.Drawing.Point(4, 22);
            this.tabServers.Name = "tabServers";
            this.tabServers.Padding = new System.Windows.Forms.Padding(3);
            this.tabServers.Size = new System.Drawing.Size(664, 634);
            this.tabServers.TabIndex = 2;
            this.tabServers.Text = "Servers";
            this.tabServers.UseVisualStyleBackColor = true;
            // 
            // tabOptions
            // 
            this.tabOptions.Controls.Add(this.tlOptions);
            this.tabOptions.Location = new System.Drawing.Point(4, 22);
            this.tabOptions.Name = "tabOptions";
            this.tabOptions.Padding = new System.Windows.Forms.Padding(3);
            this.tabOptions.Size = new System.Drawing.Size(664, 634);
            this.tabOptions.TabIndex = 4;
            this.tabOptions.Text = "Options";
            this.tabOptions.UseVisualStyleBackColor = true;
            // 
            // tlOptions
            // 
            this.tlOptions.ColumnCount = 1;
            this.tlOptions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlOptions.Controls.Add(this.tlProtonOption, 0, 0);
            this.tlOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlOptions.Location = new System.Drawing.Point(3, 3);
            this.tlOptions.Name = "tlOptions";
            this.tlOptions.RowCount = 2;
            this.tlOptions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlOptions.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlOptions.Size = new System.Drawing.Size(658, 628);
            this.tlOptions.TabIndex = 0;
            // 
            // tlProtonOption
            // 
            this.tlProtonOption.ColumnCount = 2;
            this.tlProtonOption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlProtonOption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlProtonOption.Controls.Add(this.btnProtonExe, 1, 0);
            this.tlProtonOption.Controls.Add(this.lblProtonExe, 0, 0);
            this.tlProtonOption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlProtonOption.Location = new System.Drawing.Point(3, 3);
            this.tlProtonOption.Name = "tlProtonOption";
            this.tlProtonOption.RowCount = 1;
            this.tlProtonOption.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlProtonOption.Size = new System.Drawing.Size(652, 34);
            this.tlProtonOption.TabIndex = 0;
            // 
            // btnProtonExe
            // 
            this.btnProtonExe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnProtonExe.Location = new System.Drawing.Point(329, 3);
            this.btnProtonExe.Name = "btnProtonExe";
            this.btnProtonExe.Size = new System.Drawing.Size(320, 28);
            this.btnProtonExe.TabIndex = 1;
            this.btnProtonExe.Text = "(none)";
            this.btnProtonExe.UseVisualStyleBackColor = true;
            // 
            // lblProtonExe
            // 
            this.lblProtonExe.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblProtonExe.AutoSize = true;
            this.lblProtonExe.Location = new System.Drawing.Point(116, 10);
            this.lblProtonExe.Name = "lblProtonExe";
            this.lblProtonExe.Size = new System.Drawing.Size(94, 13);
            this.lblProtonExe.TabIndex = 0;
            this.lblProtonExe.Text = "Proton Executable";
            // 
            // cbEnableGm
            // 
            this.cbEnableGm.AutoSize = true;
            this.cbEnableGm.Location = new System.Drawing.Point(3, 26);
            this.cbEnableGm.Name = "cbEnableGm";
            this.cbEnableGm.Size = new System.Drawing.Size(150, 17);
            this.cbEnableGm.TabIndex = 8;
            this.cbEnableGm.Text = "Enable Global Mobilization";
            this.cbEnableGm.UseVisualStyleBackColor = true;
            this.cbEnableGm.CheckedChanged += new System.EventHandler(this.CbEnableGm_CheckedChanged);
            // 
            // cbEnableContact
            // 
            this.cbEnableContact.AutoSize = true;
            this.cbEnableContact.Location = new System.Drawing.Point(3, 49);
            this.cbEnableContact.Name = "cbEnableContact";
            this.cbEnableContact.Size = new System.Drawing.Size(99, 17);
            this.cbEnableContact.TabIndex = 9;
            this.cbEnableContact.Text = "Enable Contact";
            this.cbEnableContact.UseVisualStyleBackColor = true;
            this.cbEnableContact.CheckedChanged += new System.EventHandler(this.CbEnableContact_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 660);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainForm";
            this.Text = "BA3L";
            this.tabControl1.ResumeLayout(false);
            this.tabPlay.ResumeLayout(false);
            this.tabPlay.PerformLayout();
            this.tlPlay.ResumeLayout(false);
            this.tlPlay.PerformLayout();
            this.tabParameters.ResumeLayout(false);
            this.tlParameters.ResumeLayout(false);
            this.tlParameters.PerformLayout();
            this.tabMods.ResumeLayout(false);
            this.tlMods.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMods)).EndInit();
            this.tlModSaveLoad.ResumeLayout(false);
            this.tabOptions.ResumeLayout(false);
            this.tlOptions.ResumeLayout(false);
            this.tlProtonOption.ResumeLayout(false);
            this.tlProtonOption.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabParameters;
        private System.Windows.Forms.TabPage tabMods;
        private System.Windows.Forms.TabPage tabServers;
        private System.Windows.Forms.Button btnLoadModPreset;
        private System.Windows.Forms.Button btnSaveModPreset;
        private System.Windows.Forms.TabPage tabPlay;
        private System.Windows.Forms.TabPage tabOptions;
        private System.Windows.Forms.TableLayoutPanel tlPlay;
        private System.Windows.Forms.Button btnPlayWithoutMods;
        private System.Windows.Forms.Button btnPlayWithMods;
        private System.Windows.Forms.CheckBox cbPlayBattlEye;
        private System.Windows.Forms.TableLayoutPanel tlOptions;
        private System.Windows.Forms.TableLayoutPanel tlProtonOption;
        private System.Windows.Forms.Label lblProtonExe;
        private System.Windows.Forms.Button btnProtonExe;
        private System.Windows.Forms.TableLayoutPanel tlParameters;
        private System.Windows.Forms.CheckBox cbNoSplash;
        private System.Windows.Forms.CheckBox cbSkipIntro;
        private System.Windows.Forms.CheckBox cbWindowed;
        private System.Windows.Forms.CheckBox cbExtraParams;
        private System.Windows.Forms.TextBox txtExtraParams;
        private System.Windows.Forms.TableLayoutPanel tlMods;
        private System.Windows.Forms.DataGridView dgvMods;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colEnabled;
        private System.Windows.Forms.DataGridViewTextBoxColumn colModName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIdentifier;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
        private System.Windows.Forms.TableLayoutPanel tlModSaveLoad;
        private System.Windows.Forms.CheckBox cbEnableGm;
        private System.Windows.Forms.CheckBox cbEnableContact;
    }
}