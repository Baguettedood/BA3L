﻿using System.Text;
using System.Text.RegularExpressions;

namespace BA3L
{
	public enum BattlEyeFilter
	{
		Any,
		Protected,
		NotProtected,
	}

	public static class ServerFilters
	{
		// https://community.bistudio.com/wiki/STEAMWORKSquery#Table

		public static BattlEyeFilter BattlEye = BattlEyeFilter.Any;
		public static bool NoPasswordRequired = false;
		public static bool NotFull;
		public static bool HasPlayers;
        public static bool SameVersion;
        public static string ServerName = "";



		public static void AddFilters(Steamworks.ServerList.Base serverQuery)
		{
			// https://partner.steamgames.com/doc/api/ISteamMatchmakingServers#MatchMakingKeyValuePair_t
			// gametagsand = Server contains all of the specified strings in a comma-delimited list.

            // Tags filters
            // BattlEye
			if(BattlEye == BattlEyeFilter.Protected)
			{
				serverQuery.AddFilter("gametagsand", "bt");
			}
			else if(BattlEye == BattlEyeFilter.NotProtected)
			{
				serverQuery.AddFilter("gametagsand", "bf");
			}

            // Version
            if(SameVersion)
            {
                // r<version number>
                // eg if version is 2.02, the tag would be r202
                // Get game version
                // Convert it into needed format
                string versiontag = "r" + Regex.Replace(Utilities.GameVersion, @"\.", "");
            }

            // As far as I know, we can't filter by password here; do this in PassesFilter.
            // Same with server name?

            // Players filters
            if(NotFull)
			{
				serverQuery.AddFilter("notfull", ""); // no value needed
			}

			if(HasPlayers)
			{
				serverQuery.AddFilter("hasplayers", ""); // no value needed
			}


		}

		public static bool PassesFilter(Server server)
		{
			if(BattlEye != BattlEyeFilter.Any)
			{
				// Protected servers only. Remove non-protected servers.
				if(BattlEye == BattlEyeFilter.Protected && !server.Tags.BattlEye)
					return false;

				// Non-protected servers only. Get rid of BattlEye-protected servers
				if(BattlEye == BattlEyeFilter.NotProtected && server.Tags.BattlEye)
					return false;
			}

            // Same Version
            if(SameVersion && server.Version != Utilities.GameVersion)
            {
                return false;
            }

            // Non-passworded servers only
            if(NoPasswordRequired && server.Passworded)
				return false;

			if(NotFull && (server.Players >= server.MaxPlayers)) // in case a server says it has 11/10 players for secret admin slots
				return false;

			if(HasPlayers && server.Players == 0)
				return false;


            // Server name
            // Probably expensive
            if(!string.IsNullOrEmpty(ServerName))
            {
                // Case-insensitive
                if(!(server.Name.IndexOf(ServerName, System.StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    return false;
                }
            }

            return true;
		}




	}
}
