﻿using System;
using System.Text;
using Steamworks;

namespace BA3L
{
	/// <summary>
	/// Manages optional DLCs: Global Mobilization and Contact
	/// </summary>
	public static class DlcManager
	{
		public const int APPID_GM = 1042220;        // Global Mobilization: Cold War Germany
		public const int APPID_CONTACT = 1021790;   // Arma 3: Contact
        public const int APPID_SOG = 1227700;       // S.O.G. Prarie Fire
        public const int APPID_CSLA = 1294440;      // CSLA Iron Curtain
        public const int APPID_WSAHARA = 1681170;   // Western Sahara

        private static bool _GmEnabled;
		public static bool GmEnabled
		{
			get { return _GmEnabled; }

			set
			{
				if(value)
				{
					if(GmInstalled)
					{
						_GmEnabled = true;
					}
				}
				else
				{
					_GmEnabled = false;
				}
			}
		}

		private static bool _ContactEnabled;
		public static bool ContactEnabled
		{ 
			get { return _ContactEnabled; }
			set
			{
				if(value)
				{
					if(ContactInstalled)
					{
						_ContactEnabled = true;
					}
				}
				else
				{
					_ContactEnabled = false;
				}
			}
		}

        private static bool _SogEnabled;
        public static bool SogEnabled
        {
            get { return _SogEnabled; }
            set
            {
                if(value)
                {
                    if(SogInstalled)
                    {
                        _SogEnabled = true;
                    }
                }
                else
                {
                    _SogEnabled = false;
                }
            }
        }

        private static bool _CslaEnabled;
        public static bool CslaEnabled
        {
            get { return _CslaEnabled; }
            set
            {
                if(value)
                {
                    if(CslaInstalled)
                    {
                        _CslaEnabled = true;
                    }
                }
                else
                {
                    _CslaEnabled = false;
                }
            }
        }

        private static bool _WSaharaEnabled;
        public static bool WSaharaEnabled
        {
            get { return _WSaharaEnabled; }
            set
            {
                if(value)
                {
                    if(WSaharaInstalled)
                    {
                        _WSaharaEnabled = true;
                    }
                }
                else
                {
                    _WSaharaEnabled = false;
                }
            }
        }

        public static bool GmInstalled => SteamApps.IsDlcInstalled(APPID_GM);
		public static bool ContactInstalled => SteamApps.IsDlcInstalled(APPID_CONTACT);
        public static bool SogInstalled => SteamApps.IsDlcInstalled(APPID_SOG);
        public static bool CslaInstalled => SteamApps.IsDlcInstalled(APPID_CSLA);
        public static bool WSaharaInstalled => SteamApps.IsDlcInstalled(APPID_WSAHARA);

		public static string GenerateDlcLaunchParameters()
		{
			StringBuilder sb = new StringBuilder();

            // Trailing space after the parameter is intended.
			if(GmEnabled)
			{
				sb.Append("-mod=GM ");
			}

			if(ContactEnabled)
			{
				sb.Append("-mod=Contact ");
			}

            if(SogEnabled)
            {
                sb.Append("-mod=vn ");
            }

            if(CslaEnabled)
            {
                sb.Append("-mod=CSLA ");
            }

            if(WSaharaEnabled)
            {
                sb.Append("-mod=WS ");
            }

            return sb.ToString();
		}
	}
}
