﻿using System;
namespace BA3L.Exceptions
{
	public class InvalidRulesException : Exception
	{
		public InvalidRulesException(string message) : base(message) { }
	}
}
