﻿using System;
namespace BA3L.Exceptions
{
	public class ModNotReadyException: Exception
	{

		public Mod FailedMod { get; protected set; }

		public ModNotReadyException() {}
		public ModNotReadyException(string message) : base(message) {}
		public ModNotReadyException(Mod failedMod)
		{
			this.FailedMod = failedMod;
		}
	}
}
