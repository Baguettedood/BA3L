﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace BA3L
{
	public class OnGameExitEventArgs : EventArgs { }


	public class Core
	{
		readonly List<string> gameArgs;
		readonly Dictionary<string, bool> launcherArgs;

		const string EXECUTABLE_32BIT = "arma3.exe";
		const string EXECUTABLE_64BIT = "arma3_x64.exe";
		const string EXECUTABLE_BATTLEYE = "arma3battleye.exe";

		// Call this event when the game exits, useful when the UI wants to do something when Arma 3 quits.
		public static EventHandler<OnGameExitEventArgs> OnGameExitEvent;

		public Core(List<string> gameArgs, Dictionary<string, bool> launcherArgs)
		{
			this.gameArgs = gameArgs;
			this.launcherArgs = launcherArgs;

			ModManager.Init();

			Settings.Load(Settings.GetLauncherSavePath());
			ModManager.LoadSavedMods();
		}

		public void Shutdown()
		{
			Settings.Save(Settings.GetLauncherSavePath(), ModManager.GetEnabledMods());
			// If needed, ask other classes to stop doing things that might be too late to stop in the destructor
			ServerBrowser.DisposeCurrentQuery(); // Use a ServerBrowser.Shutdown() instead?
		}

		public class PlayOptions
		{
			public bool Force64Bit;
			public bool Force32Bit;
			public bool ForceBattlEye;
			public bool ForceRunViaSteam; // Linux: Force BA3L to launch Arma 3 via Steam instead of via Proton's executable
			public bool IncludeLaunchOptions; // Include launcher's launch options
			public bool PlayWithMods;
			public Server JoinServer;
			public string ServerPassword = "";
		};

		public bool JoinServer(Server server, string password = "") // Assume server is otherwise valid?
		{
			if(server == null)
			{
				return false;
			}
			PlayOptions playOptions = new PlayOptions();
			playOptions.IncludeLaunchOptions = true;
			playOptions.PlayWithMods = true;
			playOptions.JoinServer = server;
			playOptions.ServerPassword = password;
			return Play(playOptions);
		}

		public bool Play() // Default play options
		{
			PlayOptions playOptions = new PlayOptions();
			playOptions.IncludeLaunchOptions = true;
			playOptions.PlayWithMods = true;
			return Play(playOptions);
		}

		public bool Play(PlayOptions playOptions)
		{
			playOptions.ForceBattlEye = Settings.PlayWithBattlEye;

			StringBuilder parameters = new StringBuilder();
			parameters.Append(GameArgsToString());
			if(playOptions.IncludeLaunchOptions)
			{
				parameters.Append(" ");
				parameters.Append(GenerateParameters());
				if(Settings.ExtraCmdLineParams != "")
				{
					parameters.Append(Settings.ExtraCmdLineParams);
					parameters.Append(" ");
				}
			}

			parameters.Append(DlcManager.GenerateDlcLaunchParameters());

			if(playOptions.PlayWithMods)
			{
				string modLaunchParameters = ModManager.GenerateModLaunchParameters();
				// There may be a maximum character limit that will be hit with too many mods' launch strings.
				// If this happens then look into using parameter files.
				// If there is a max limit, I haven't reached it yet.
				parameters.Append(@modLaunchParameters);
			}
            else
            {
                parameters.Append("-mod=\"\"");
            }

            if(playOptions.JoinServer != null)
			{
				parameters.Append($" -connect={playOptions.JoinServer.Address}");
				parameters.Append($" -port={playOptions.JoinServer.ConnectionPort}");
				if(playOptions.ServerPassword.Length != 0)
				{
					parameters.Append($" -password={playOptions.ServerPassword}");
				}
			}

			// TODO figure out what extra features are needed.
			if(Utilities.GetOS() == Utilities.OS.OS_WINDOWS)
			{
				return PlayWindows(playOptions, parameters);
			}
			else if(Utilities.GetOS() == Utilities.OS.OS_LINUX)
			{
				if(Utilities.IsProton() && !playOptions.ForceRunViaSteam)
				{
					return PlayProton(playOptions, parameters);
				}
				else
				{
					return PlayLinuxViaSteam(parameters);
				}
			}
			else if(Utilities.GetOS() == Utilities.OS.OS_OSX)
			{
				throw new Exceptions.UnsupportedOperatingSystemException("MacOS not supported yet");
			}
			else
			{
				throw new Exceptions.UnsupportedOperatingSystemException("Unknown Operating System!");
			}

		}

		private static bool PlayLinuxViaSteam(StringBuilder parameters)
		{
			// When playing via Steam, WaitForExit will wait for the Steam process to exit rather than Arma 3.
			Process p = Process.Start("/usr/bin/steam", "-applaunch 107410 " + @parameters + " -noLauncher");
			ConfigureExitEvent(p);

			return true;
		}

		private static bool PlayWindows(PlayOptions playOptions, StringBuilder parameters)
		{
			string executable = GetWindowsExecutable(playOptions);
			string fullExePath = Path.Combine(Utilities.Arma3Path, executable);

			if(File.Exists(fullExePath))
			{
				Process p = Process.Start(fullExePath, parameters.ToString());
				ConfigureExitEvent(p);
				return true;
			}

			return false; // Executable doesn't exist if we reach here
		}

		private static string GetWindowsExecutable(PlayOptions playOptions)
		{
			string executable;

			if(playOptions.ForceBattlEye && Utilities.GetOS() == Utilities.OS.OS_WINDOWS)
			{
                // Ignore ForceBattlEye if not running on Windows.
                // Proton BattlEye Runtime will load BattlEye on its own.
                // Consider expanding this later to detect if BA3L is running in Wine?
				executable = EXECUTABLE_BATTLEYE;
			}
			else if((Environment.Is64BitOperatingSystem && !playOptions.Force32Bit) || playOptions.Force64Bit)
			{
				executable = EXECUTABLE_64BIT;
			}
			else
			{
				executable = EXECUTABLE_32BIT;
			}

			return executable;
		}

		private static bool PlayProton(PlayOptions playOptions, StringBuilder parameters)
		{
			// Assume this method is only called from a Proton-compatible OS.
			// Run Proton if the executable is found
			if(!File.Exists(Utilities.GetProtonPath()))
			{
				throw new Exceptions.ProtonNotFoundException("Proton path not valid!");
			}

			// If we get here, Proton's executable should hopefully be valid.
			string executable = GetWindowsExecutable(playOptions);
			string fullExePath = Path.Combine(Utilities.Arma3Path, executable);
			if(File.Exists(fullExePath))
			{
				// Set working directory
				ProcessStartInfo startInfo = new ProcessStartInfo();
				startInfo.WorkingDirectory = Utilities.Arma3Path;
				startInfo.Arguments = "run \"" + fullExePath + "\" " + parameters;
                startInfo.FileName = Utilities.GetProtonPath();
				// Env vars
				string compatDataPath = Path.GetFullPath(Path.Combine(
					Utilities.Arma3Path, "..", "..", "compatdata",
					Utilities.ARMA3_APPID.ToString())
				);
				// Proton's way of setting Wine prefixes when calling the proton executable.
				startInfo.EnvironmentVariables["STEAM_COMPAT_DATA_PATH"] = compatDataPath;

                // Steam normally sets this, so we have to do it manually.
                // Required for Proton 6.3 and up
                startInfo.EnvironmentVariables["STEAM_COMPAT_CLIENT_INSTALL_PATH"] = Utilities.SteamPath;

                // Undocumented Proton env var required for steam-<AppId>.log in home dir if we have PROTON_LOG=1
                startInfo.EnvironmentVariables["SteamGameId"] = Utilities.ARMA3_APPID.ToString();
                startInfo.EnvironmentVariables["SteamAppId"] = Utilities.ARMA3_APPID.ToString();

                // LD_PRELOAD the Steam Overlay
                // This probably won't work with Flatpak
                string oldPreload = Environment.GetEnvironmentVariable("LD_PRELOAD");
				string localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
				string steamDir = Path.Combine(localAppData, "Steam");
				string newPreload = steamDir + "/ubuntu12_32/gameoverlayrenderer.so:" + steamDir + "/ubuntu12_64/gameoverlayrenderer.so";
				if(oldPreload != "")
				{
					newPreload = oldPreload + ":" + newPreload;
				}
				startInfo.EnvironmentVariables["LD_PRELOAD"] = newPreload;

				startInfo.UseShellExecute = false;


				// TODO this will probably die if we close the launcher while Arma is running.
				Process p = Process.Start(startInfo);
				ConfigureExitEvent(p);
				return true;
			}


			return false;
		}

		private string GameArgsToString()
		{
			return Utilities.ArgsToString(gameArgs.ToArray());
		}

		private string GenerateParameters()
		{
			StringBuilder parameters = new StringBuilder();

			// TODO go through more applicable settings and include them if needed.
			if(Settings.NoSplash)
				parameters.Append("-nosplash ");
			if (Settings.SkipIntro)
				parameters.Append("-skipIntro ");
			if (Settings.Windowed)
				parameters.Append("-window ");

			return parameters.ToString();
		}

		private static void ConfigureExitEvent(Process p)
		{
			p.EnableRaisingEvents = true;
			// I guess Sender would be the process
			p.Exited += (sender, e) => OnGameExitEvent?.Invoke(sender, new OnGameExitEventArgs());
		}

		public void ApplyModChangesAndJoin(Server server, string password, List<MatchedMod> matchedMods)
		{
			Task.Run(() => ApplyModChangesAndJoinAsync(server, password, matchedMods)).ConfigureAwait(false);
		}

		private async Task ApplyModChangesAndJoinAsync(Server server, string password, List<MatchedMod> matchedMods)
		{
			// Perform all Current Actions in matchedMods
			foreach(MatchedMod matched in matchedMods)
			{
				await PerformAction(matched.CurrentAction);
			}

			// TODO figure a way to lock user out of changing mod list
			// TODO feedback to user on what is happening

			// Wait until mods are all ready
			// I don't think we can wait for an event
			// TODO a way to cancel this join
			// TODO exception handling and figure out a way to send the exception back up to the GUI
			while(!ModManager.EnabledModsReady)
			{
				await Task.Delay(1000);
			}

			// Join the server
			JoinServer(server, password);
		}

		private async Task PerformAction(ModActions.MatchedModAction action)
		{
			if(action.GetType() == typeof(ModActions.KeepEnabled))
			{
				// Don't really need to do anything
			}
			else if(action.GetType() == typeof(ModActions.KeepDisabled))
			{
				// Do nothing
			}
			else if(action.GetType() == typeof(ModActions.EnableMod))
			{
				ModManager.EnableMod((action as ModActions.EnableMod).ModToEnable);
			}
			else if(action.GetType() == typeof(ModActions.DisableMod))
			{
				ModManager.DisableMod((action as ModActions.DisableMod).ModToDisable);
			}
			else if(action.GetType() == typeof(ModActions.SubscribeToWorkshopMod))
			{
				// TODO consider not awaiting this.
				await ModManager.DownloadWorkshopMod((action as ModActions.SubscribeToWorkshopMod).WorkshopId, true);
			}
			else if(action.GetType() == typeof(ModActions.DoNothing))
			{
				// Do nothing again
			}
			else
			{
				// Well then we have a problem
			}
		}
	}
}
