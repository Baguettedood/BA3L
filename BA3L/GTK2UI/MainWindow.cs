﻿using System;
using System.Collections.Generic;
using BA3L;
using Gtk;
using System.Linq;

public partial class MainWindow : Gtk.Window
{
	Core core;
	ListStore tvModListStore;

	public MainWindow(Core core) : base(Gtk.WindowType.Toplevel)
	{
		// TODO save/load the window's position and size?
		Build();
		this.core = core;

		UpdateParameterValues();
		SetupModList();
		UpdateModList();
		ModManager.RefreshModsEvent += OnUpdateModList;
		Core.OnGameExitEvent += OnGameExitEvent;

		UpdateSettingValues();

		btnEnableGm.Sensitive = DlcManager.GmInstalled;
		btnEnableContact.Sensitive = DlcManager.ContactInstalled;
        btnEnableSog.Sensitive = DlcManager.SogInstalled;
        btnEnableCsla.Sensitive = DlcManager.CslaInstalled;
        btnEnableWSahara.Sensitive = DlcManager.WSaharaInstalled;

		SetupServerBrowser();
	}

	private void OnGameExitEvent(object sender, OnGameExitEventArgs e)
	{
		Application.Invoke(delegate {
			// TODO do something when the game exits for this and WinForms UI.
			Console.WriteLine("Game exited.");
		});
	}


	protected void OnDeleteEvent(object sender, DeleteEventArgs a)
	{
		Core.OnGameExitEvent -= OnGameExitEvent;
		Application.Quit();
		a.RetVal = true;
	}

	protected void Play(Core.PlayOptions playOptions)
	{
		try
		{
			core.Play(playOptions);
		}
		catch(BA3L.Exceptions.ModNotReadyException ex)
		{
			string message = "One or more mods are not ready.\nFirst failed mod: " + ex.FailedMod.Name;
			MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent,
				MessageType.Error, ButtonsType.Ok, message);
			md.WindowPosition = WindowPosition.Center;
			md.Run();
			md.Destroy();
		}
		catch(BA3L.Exceptions.ProtonNotFoundException ex)
		{
			string message = "Proton not found: " + ex.Message
				+ "\nSet the path to your Proton executable in Steam, or contact a developer.";
			MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent,
				MessageType.Error, ButtonsType.Ok, message);
			md.WindowPosition = WindowPosition.Center;
			md.Run();
			md.Destroy();
		}
	}

	protected void OnBtnPlayWithModsClicked(object sender, EventArgs e)
	{
		Core.PlayOptions playOptions = new Core.PlayOptions();
		playOptions.IncludeLaunchOptions = true;
		playOptions.PlayWithMods = true;
		Play(playOptions);
	}

	protected void OnBtnPlayWithoutModsClicked(object sender, EventArgs e)
	{
		Core.PlayOptions playOptions = new Core.PlayOptions();
		playOptions.IncludeLaunchOptions = true;
		playOptions.PlayWithMods = false;
		Play(playOptions);
	}

	protected void OnNotebook1SwitchPage(object o, SwitchPageArgs args)
	{
		// This seems wrong but I don't know a better signal when we open the contents of a page.
		Widget w = notebook1.GetNthPage((int)args.PageNum);
		if(w.Name == "vboxParameters")
		{
			UpdateParameterValues();
		}
		else if(w.Name == "swModList")
		{
			Console.WriteLine("Mod list selected");
		}
		else if(w.Name == "vboxServerBrowser")
		{
			OnServerBrowserFocus();
		}
	}

	private void SetupModList()
	{
		// Columns:
		// Mod Enabled, Mod Name, Mod Identifier
		TreeViewColumn enabledColumn = new TreeViewColumn();
		enabledColumn.Title = "Enabled";
		CellRendererToggle enableCell = new CellRendererToggle();
		enableCell.Toggled += ModEnableToggled;
		enabledColumn.PackStart(enableCell, true);

		TreeViewColumn modNameColumn = new TreeViewColumn();
		modNameColumn.Title = "Mod name";
		CellRendererText nameCell = new CellRendererText();
		modNameColumn.PackStart(nameCell, true);
		modNameColumn.Expand = true;

		TreeViewColumn identifierColumn = new TreeViewColumn();
		identifierColumn.Title = "Identifier";
		CellRendererText identifierCell = new CellRendererText();
		identifierColumn.PackStart(identifierCell, true);

		TreeViewColumn statusColumn = new TreeViewColumn();
		statusColumn.Title = "Status";
		CellRendererText statusCell = new CellRendererText();
		statusColumn.PackStart(statusCell, true);

		// Cell data funcs
		enabledColumn.SetCellDataFunc(enableCell, new TreeCellDataFunc(RenderModEnabled));
		modNameColumn.SetCellDataFunc(nameCell, new TreeCellDataFunc(RenderModName));
		identifierColumn.SetCellDataFunc(identifierCell, new TreeCellDataFunc(RenderModIdentifier));
		statusColumn.SetCellDataFunc(statusCell, new TreeCellDataFunc(RenderModStatus));

		ListStore store = new ListStore(typeof(Mod));

		tvModList.Model = store;
		tvModListStore = store; // Do we "need" to store this if we can get it from tvModList.Model?

		tvModList.AppendColumn(enabledColumn);
		tvModList.AppendColumn(modNameColumn);
		tvModList.AppendColumn(identifierColumn);
		tvModList.AppendColumn(statusColumn);
	}

	private void RenderModEnabled(TreeViewColumn column, CellRenderer cell, TreeModel model, TreeIter iter)
	{
		Mod mod = (Mod)model.GetValue(iter, 0);
		if(mod != null)
		{
			(cell as CellRendererToggle).Active = mod.Enabled;
		}
		else
		{
			Console.Error.WriteLine("MainWindow - RenderModEnabled - Unknown mod!");
			(cell as CellRendererToggle).Active = false;
		}
	}

	private void RenderModName(TreeViewColumn column, CellRenderer cell, TreeModel model, TreeIter iter)
	{
		Mod mod = (Mod)model.GetValue(iter, 0);
		if(mod != null)
		{
			(cell as CellRendererText).Text = mod.Name;
		}
		else
		{
			Console.Error.WriteLine("MainWindow - RenderModName - Unknown mod!");
			(cell as CellRendererText).Text = "UNKNOWN MOD";
		}
	}

	private void RenderModIdentifier(TreeViewColumn column, CellRenderer cell, TreeModel model, TreeIter iter)
	{
		Mod mod = (Mod)model.GetValue(iter, 0);
		if(mod != null)
		{
			(cell as CellRendererText).Text = mod.Identifier;
		}
		else
		{
			Console.Error.WriteLine("MainWindow - RenderModIdentifier - Unknown mod!");
			(cell as CellRendererText).Text = "UNKNOWN ID";
		}
	}

	private void RenderModStatus(TreeViewColumn column, CellRenderer cell, TreeModel model, TreeIter iter)
	{
		Mod mod = (Mod)model.GetValue(iter, 0);
		if(mod != null)
		{
			(cell as CellRendererText).Text = mod.Status;
		}
		else
		{
			Console.Error.WriteLine("MainWindow - RenderModStatus - Unknown mod!");
			(cell as CellRendererText).Text = "UNKNOWN STATUS";
		}
	}

	private void ModEnableToggled(object o, ToggledArgs args)
	{
		TreeIter iter;

		if(tvModListStore.GetIter(out iter, new TreePath(args.Path)))
		{
			var toggledMod = (Mod)tvModListStore.GetValue(iter, 0);
			if(toggledMod != null)
			{
				ModManager.ToggleMod(toggledMod);
			}
		}
	}

	private void UpdateModList()
	{
		tvModListStore.Clear();

		List<Mod> sortedMods = new List<Mod>(ModManager.GetMods());
		sortedMods.Sort(new ModNameComparer<Mod>());

		foreach(Mod mod in sortedMods)
		{
			tvModListStore.AppendValues(mod);
		}
	}

	private void OnUpdateModList(object sender, RefreshModsEventArgs e)
	{
		Application.Invoke(delegate
		{
			UpdateModList();
		});
	}

	private void UpdateParameterValues()
	{
		// TODO more parameters
		btnNoSplash.Active = BA3L.Settings.NoSplash;
		btnSkipIntro.Active = BA3L.Settings.SkipIntro;
		btnWindowed.Active = BA3L.Settings.Windowed;
		teExtraParams.Text = BA3L.Settings.ExtraCmdLineParams;
		cbExtraParams.Active = teExtraParams.Text.Length > 0;
	}

	private void UpdateSettingValues()
	{
		// Play tab settings (BattlEye)
		btnPlayBattlEye.Active = BA3L.Settings.PlayWithBattlEye;
		btnEnableGm.Active = DlcManager.GmEnabled;
		btnEnableContact.Active = DlcManager.ContactEnabled;
        btnEnableSog.Active = DlcManager.SogEnabled;
        btnEnableCsla.Active = DlcManager.CslaEnabled;
        btnEnableWSahara.Active = DlcManager.WSaharaEnabled;
	}

	protected void OnBtnNoSplashToggled(object sender, EventArgs e)
	{
		// "Toggled" seems to be called when their value is changed
		// in UpdateParameterValues.
		BA3L.Settings.NoSplash = btnNoSplash.Active;
	}

	protected void OnBtnSkipIntroToggled(object sender, EventArgs e)
	{
		BA3L.Settings.SkipIntro = btnSkipIntro.Active;
	}

	protected void OnBtnWindowedToggled(object sender, EventArgs e)
	{
		BA3L.Settings.Windowed = btnWindowed.Active;
	}

	protected void OnBtnPlayBattlEyeToggled(object sender, EventArgs e)
	{
		BA3L.Settings.PlayWithBattlEye = btnPlayBattlEye.Active;
	}

	protected void OnBtnSaveModPresetClicked(object sender, EventArgs e)
	{
		// Open file chooser to save
		FileChooserDialog fc =
			new FileChooserDialog(
				"Save Preset",
				this,
				FileChooserAction.Save,
				"Cancel", ResponseType.Cancel,
				"Save", ResponseType.Accept
			);

		fc.Filter = new FileFilter();
		fc.Filter.AddPattern("*" + BA3L.Settings.EXT_BA3LSAVE);
		fc.SetCurrentFolder(BA3L.Settings.GetLauncherSaveDirectory());
		fc.CurrentName = "preset" + BA3L.Settings.EXT_BA3LSAVE;
		if(fc.Run() == (int)ResponseType.Accept)
		{
			BA3L.Settings.SaveModPresetFile(fc.Filename, ModManager.GetEnabledMods());
		}

		fc.Destroy();
	}

	protected void OnBtnLoadModPresetClicked(object sender, EventArgs e)
	{
		// Open file chooser to load
		FileChooserDialog fc =
			new FileChooserDialog(
				"Load preset",
				this,
				FileChooserAction.Open,
				"Cancel", ResponseType.Cancel,
				"Load", ResponseType.Accept
			);

		fc.Filter = new FileFilter();
		fc.Filter.AddPattern($"*{BA3L.Settings.EXT_BA3LSAVE}");
		fc.Filter.AddPattern($"*{BA3L.Settings.EXT_HTML}");
		fc.Filter.AddPattern($"*{BA3L.Settings.EXT_A3ULML}");
		fc.SetCurrentFolder(BA3L.Settings.GetLauncherSaveDirectory());
		if(fc.Run() == (int)ResponseType.Accept)
		{
			// Load file
			BA3L.Settings.LoadModPresetFile(fc.Filename);
			// Force UI to update now
			tvModList.QueueDraw();
		}

		fc.Destroy();
	}

	protected void OnTeExtraParamsChanged(object sender, EventArgs e)
	{
		cbExtraParams.Active = teExtraParams.Text.Length > 0;
		// Wanted to do this on a "finished editing" event but it didn't do anything
		BA3L.Settings.ExtraCmdLineParams = teExtraParams.Text;
	}

	protected void OnBtnEnableGmToggled(object sender, EventArgs e)
	{
		DlcManager.GmEnabled = btnEnableGm.Active;
	}

	protected void OnBtnEnableContactToggled(object sender, EventArgs e)
	{
		DlcManager.ContactEnabled = btnEnableContact.Active;
	}

    protected void OnBtnEnableSogToggled(object sender, EventArgs e)
    {
        DlcManager.SogEnabled = btnEnableSog.Active;
    }

    protected void OnBtnEnableCslaToggled(object sender, EventArgs e)
    {
        DlcManager.CslaEnabled = btnEnableCsla.Active;
    }

    protected void OnBtnEnableWSaharaToggled(object sender, EventArgs e)
    {
        DlcManager.WSaharaEnabled = btnEnableWSahara.Active;
    }

    #region ServerBrowser
    private bool ServerBrowserFocused = false;
	ListStore tvServerListStore;
	TreeModelFilter tvServerListFilter;
	TreeModelSort tvServerListFilterSort;

	Gdk.Pixbuf LockIcon = Gdk.Pixbuf.LoadFromResource("BA3L.GTK2UI.Icons.lock.lock.png");
	Gdk.Pixbuf ShieldIcon = Gdk.Pixbuf.LoadFromResource("BA3L.GTK2UI.Icons.shield.computer-security-shield.png");

	private Server SelectedServer = null;

	// Server Browser
	private void SetupServerBrowser()
	{
		ServerBrowser.OnServerRefreshCompleteEvent += OnServerRefreshComplete;
		ServerBrowser.OnInternetServerRespondEvent += OnInternetServerRespond;
		ServerBrowser.OnOtherServerRespondEvent += OnOtherServerRespond;

		// Setup TreeView
		// https://community.bistudio.com/wikidata/images/a/a8/Launcher_ServerRow.png
		// https://community.bistudio.com/wikidata/images/2/2f/Laiuncher_ServerBrowser.png
		/* Columns: Favorite toggle,
		 Passworded,
		 BattlEye,
		 Green thing (signatures/mods?)
		 Name, 
		 Shield thing (unit?),
		 Game type,
		 Mission,
		 Players,
		 Ping,
		 Join,
		 Expander
		*/
		// Official launcher has mods in the expander
		// Steam server browser: Pass, Anticheat, Name, Mission, Players, Map, Ping
		// More options on right-click

		// TODO sorting, click events, filters

		// IDK how to do icons for treeview column names, other than unicode which crashes?
		TreeViewColumn passwordedColumn = new TreeViewColumn();
		passwordedColumn.Title = " ";
		CellRendererPixbuf passwordCell = new CellRendererPixbuf();
		passwordedColumn.PackStart(passwordCell, true);
		passwordedColumn.SortColumnId = 0;
		

		TreeViewColumn battlEyeColumn = new TreeViewColumn();
		battlEyeColumn.Title = "️ ";
		CellRendererPixbuf battlEyeCell = new CellRendererPixbuf();
		battlEyeColumn.PackStart(battlEyeCell, true);
		battlEyeColumn.SortColumnId = 1;

		TreeViewColumn nameColumn = new TreeViewColumn();
		nameColumn.Title = "Name";
		CellRendererText nameCell = new CellRendererText();
		nameColumn.PackStart(nameCell, true);
		nameColumn.SortColumnId = 2;
		nameColumn.Expand = true;

		TreeViewColumn missionColumn = new TreeViewColumn();
		missionColumn.Title = "Mission";
		CellRendererText missionCell = new CellRendererText();
		missionColumn.PackStart(missionCell, true);
		missionColumn.SortColumnId = 3;
		missionColumn.Expand = true;

		TreeViewColumn playersColumn = new TreeViewColumn();
		playersColumn.Title = "Players";
		CellRendererText playersCell = new CellRendererText();
		playersColumn.PackStart(playersCell, true);
		playersColumn.SortColumnId = 4;

		TreeViewColumn pingColumn = new TreeViewColumn();
		pingColumn.Title = "Ping";
		CellRendererText pingCell = new CellRendererText();
		pingColumn.PackStart(pingCell, true);
		pingColumn.SortColumnId = 5;


		// Cell data funcs
		passwordedColumn.SetCellDataFunc(passwordCell, new TreeCellDataFunc(RenderServerPassword));
		battlEyeColumn.SetCellDataFunc(battlEyeCell, new TreeCellDataFunc(RenderServerBattlEye));
		nameColumn.SetCellDataFunc(nameCell, new TreeCellDataFunc(RenderServerName));
		missionColumn.SetCellDataFunc(missionCell, new TreeCellDataFunc(RenderServerMission));
		playersColumn.SetCellDataFunc(playersCell, new TreeCellDataFunc(RenderServerPlayers));
		pingColumn.SetCellDataFunc(pingCell, new TreeCellDataFunc(RenderServerPing));



		ListStore store = new ListStore(typeof(Server));
		tvServerList.Model = store;
		tvServerListStore = store;

		tvServerList.AppendColumn(passwordedColumn);
		tvServerList.AppendColumn(battlEyeColumn);
		tvServerList.AppendColumn(nameColumn);
		tvServerList.AppendColumn(missionColumn);
		tvServerList.AppendColumn(playersColumn);
		tvServerList.AppendColumn(pingColumn);

		tvServerList.ButtonPressEvent += OnServerListButtonPress;
		tvServerList.KeyPressEvent += OnServerListKeyPress;
		tvServerList.Selection.Changed += OnServerListSelectionChanged;

		tvServerListStore.SetSortFunc(5, new TreeIterCompareFunc(ServerListSortByPing));
		tvServerListStore.SetSortColumnId(5, SortType.Ascending);

		tvServerListFilter = new TreeModelFilter(tvServerListStore, null);
		tvServerListFilter.VisibleFunc = new TreeModelFilterVisibleFunc(FilterServerList);
		//tvServerList.Model = filter;
		// TODO this filter won't do anything until we set it as the model for the tree view.
		// which might take some refactoring.
		// Note: Call filter.Refilter() when updating view preferences
		// Note 2: TreeModelFilter doesn't implement TreeSortable so we might not be able to sort.
		// Look into: https://stackoverflow.com/questions/47418386/how-do-i-sort-a-gtktreemodelfilter
		// and https://bloerg.net/2012/10/23/sorted-and-filtered-tree-view.html
		tvServerListFilterSort = new TreeModelSort(tvServerListFilter);
		// Just put another layer on!
		tvServerListFilterSort.SetSortFunc(5, new TreeIterCompareFunc(ServerListSortByPing));
		tvServerListFilterSort.SetSortColumnId(5, SortType.Ascending);
		tvServerList.Model = tvServerListFilterSort;

	}

	private bool FilterServerList(TreeModel model, TreeIter iter)
	{
		return (Server)model.GetValue(iter, 0) != null && ServerFilters.PassesFilter((Server)model.GetValue(iter, 0));
	}

	int ServerListSortByPing(TreeModel model, TreeIter a, TreeIter b)
	{
		Server s1 = (Server)model.GetValue(a, 0);
		Server s2 = (Server)model.GetValue(b, 0);
		// Negative: First server has lower ping than the second
		// Positive: First server has bigger ping.
		// 0 of course means the same
		return s1.Ping - s2.Ping;
	}


	private void OnServerListSelectionChanged(object sender, EventArgs e)
	{
		TreeModel temp1;
		TreeIter iter;
		if(tvServerList.Selection.GetSelected(out temp1, out iter))
		{
			Server s = (Server)tvServerList.Model.GetValue(iter, 0);
			SelectedServer = s;
			if(s != null)
			{
				Console.WriteLine($"Selected server: {s.Name}");
			}
		}
		else
		{
			Console.WriteLine("GetSelected false");
			SelectedServer = null;
		}

		UpdateServerConnectDetailsButtons();
	}

	[GLib.ConnectBefore]
	private void OnServerListKeyPress(object o, KeyPressEventArgs args)
	{
		if(args.Event.Key == Gdk.Key.Return || args.Event.Key == Gdk.Key.KP_Enter)
		{
			Console.WriteLine($"Key Press Event: {args.Event.Key.ToString()}");
		}
	}

	[GLib.ConnectBefore] // Need this to handle the button press event before GTK handles it itself.
	private void OnServerListButtonPress(object o, ButtonPressEventArgs args)
	{
		// Note: This event happens before the TreeView.Selection.Changed event.
		if(args.Event.Button == 1 && args.Event.Type == Gdk.EventType.TwoButtonPress) // double left click
		{
			Console.WriteLine("Double-click");
		}

		if(args.Event.Button == 3) // 3 = right-click for some reason
		{
			Console.WriteLine("Right-click");
		}
	}

	private void RenderServerPassword(TreeViewColumn tree_column, CellRenderer cell, TreeModel tree_model, TreeIter iter)
	{
		Server server = (Server)tree_model.GetValue(iter, 0);
		if(server != null && server.Passworded)
		{
			(cell as CellRendererPixbuf).Pixbuf = LockIcon;
		}
		else
		{
			(cell as CellRendererPixbuf).Pixbuf = null;
		}
	}

	private void RenderServerBattlEye(TreeViewColumn tree_column, CellRenderer cell, TreeModel tree_model, TreeIter iter)
	{
		Server server = (Server)tree_model.GetValue(iter, 0);
		if(server != null && server.Tags.BattlEye)
		{
			(cell as CellRendererPixbuf).Pixbuf = ShieldIcon;
		}
		else
		{
			(cell as CellRendererPixbuf).Pixbuf = null;
		}
	}

	private void RenderServerName(TreeViewColumn tree_column, CellRenderer cell, TreeModel tree_model, TreeIter iter)
	{
		Server server = (Server)tree_model.GetValue(iter, 0);
		if(server != null)
		{
			(cell as CellRendererText).Text = server.Name;
		}
		else
		{
			(cell as CellRendererText).Text = "";
		}
	}

	private void RenderServerMission(TreeViewColumn tree_column, CellRenderer cell, TreeModel tree_model, TreeIter iter)
	{
		// Received some sort of crash in GetValue for some reason that goes all the way to the Mono runtime
		Server server = (Server)tree_model.GetValue(iter, 0);
		if(server != null)
		{
			(cell as CellRendererText).Text = server.Mission;
		}
		else
		{
			(cell as CellRendererText).Text = "";
		}
	}

	private void RenderServerPlayers(TreeViewColumn tree_column, CellRenderer cell, TreeModel tree_model, TreeIter iter)
	{
		Server server = (Server)tree_model.GetValue(iter, 0);
		if(server != null)
		{
			(cell as CellRendererText).Text = $"{server.Players}/{server.MaxPlayers}";
		}
		else
		{
			(cell as CellRendererText).Text = "?/?";
		}
	}

	private void RenderServerPing(TreeViewColumn tree_column, CellRenderer cell, TreeModel tree_model, TreeIter iter)
	{
		Server server = (Server)tree_model.GetValue(iter, 0);
		if(server != null)
		{
			(cell as CellRendererText).Text = server.Ping.ToString();
		}
		else
		{
			(cell as CellRendererText).Text = "?";
		}
	}


	object serverListLockObj = new object();
	int serverListRefreshNum = 0;

	void OnInternetServerRespond(object sender, OnServerRespondEventArgs e)
	{
		int lastServerListRefresh = 0;
		lock(serverListLockObj)
		{
			lastServerListRefresh = serverListRefreshNum;
		}

		Application.Invoke(delegate {
			lock(serverListLockObj)
			{
				if(lastServerListRefresh == serverListRefreshNum)
				{
					tvServerListStore.AppendValues(e.RespondedServer);
					tvServerList.QueueDraw();
				}
			}
		});
	}

	void OnOtherServerRespond(object sender, OnServerRespondEventArgs e)
	{
		int lastServerListRefresh = 0;
		lock(serverListLockObj)
		{
			lastServerListRefresh = serverListRefreshNum;
		}

		Application.Invoke(delegate {
			lock(serverListLockObj)
			{
				if(lastServerListRefresh == serverListRefreshNum)
				{
					tvServerListStore.AppendValues(e.RespondedServer);
					tvServerList.QueueDraw();
				}
			}
		});
	}

	private void OnServerRefreshComplete(object sender, OnServerRefreshCompleteEventArgs e)
	{
		// TODO maybe change Refresh between "Refresh" and "Stop Refresh"?
		Console.WriteLine("Server refresh complete.");
	}

	private void ClearServerListStore()
	{
		lock(serverListLockObj)
		{
			serverListRefreshNum++;
			tvServerListStore.Clear();
			tvServerList.QueueDraw();
		}

	}

	// When the server browser tab is switched into view.
	private void OnServerBrowserFocus()
	{
		// If it's the first time, we will perform a refresh
		if(!ServerBrowserFocused)
		{
			ServerBrowserFocused = true;

			// TODO this properly, right now assume Internet.
			ClearServerListStore();
			ServerBrowser.RefreshInternetServers();
		}
	}

	protected void OnRbServerInternetToggled(object sender, EventArgs e)
	{
		// Toggled gets called if it's toggled on or off.
		if(!ServerBrowserFocused)
			return;

		if(!rbServerInternet.Active)
			return;

        UpdateServerBrowserButtons();
        // Just reuse the previous (probably cancelled) Internet query if we can.
        // Otherwise refresh instantly.
        ServerBrowser.DisposeCurrentQuery();
		ClearServerListStore();
		var InternetServers = ServerBrowser.GetInternetServerList();
		if(InternetServers.Count != 0)
		{
			// Just put them back in the server list store
			foreach(var server in InternetServers)
			{
				tvServerListStore.AppendValues(server);
			}

			tvServerList.QueueDraw();
		}
		else
		{
			RefreshInternetServers();
		}
	}

	protected void OnRbServerFavoritesToggled(object sender, EventArgs e)
	{
		if(!rbServerFavorites.Active)
			return;
		if(!ServerBrowserFocused)
			return;

        UpdateServerBrowserButtons();
        RefreshFavoriteServers();
	}

	protected void OnRbServerFriendsToggled(object sender, EventArgs e)
	{
		if(!rbServerFriends.Active)
			return;
		if(!ServerBrowserFocused)
			return;

        UpdateServerBrowserButtons();
        RefreshFriendsServers();
	}

	protected void OnRbServerHistoryToggled(object sender, EventArgs e)
	{
		if(!rbServerHistory.Active)
			return;
		if(!ServerBrowserFocused)
			return;

        UpdateServerBrowserButtons();
        RefreshHistoryServers();
	}

	protected void OnRbServerLANToggled(object sender, EventArgs e)
	{
		if(!rbServerLan.Active)
			return;
		if(!ServerBrowserFocused)
			return;

        UpdateServerBrowserButtons();
		RefreshLANServers();
	}

    private void UpdateServerBrowserButtons()
    {
        if(rbServerFavorites.Active)
        {
            btnAddServerToFavorites.Label = "Remove From Favorites";
        }
        else
        {
            btnAddServerToFavorites.Label = "Add To Favorites";
        }
    }

    private void RefreshInternetServers()
	{
		// Start a new query now
		Console.WriteLine("Refreshing Internet");
		ServerBrowser.RefreshInternetServers();
		ClearServerListStore();
	}

	private void RefreshFavoriteServers()
	{
		Console.WriteLine("Refreshing favourites");
		ServerBrowser.RefreshFavouriteServers();
		ClearServerListStore();
	}

	private void RefreshFriendsServers()
	{
		Console.WriteLine("Refreshing friends");
		ServerBrowser.RefreshFriendsServers();
		ClearServerListStore();
	}

	private void RefreshHistoryServers()
	{
		Console.WriteLine("Refreshing history");
		ServerBrowser.RefreshRecentServers();
		ClearServerListStore();
	}

	private void RefreshLANServers()
	{
		Console.WriteLine("Refreshing LAN");
		ServerBrowser.RefreshLANServers();
		ClearServerListStore();
	}


	protected void OnBtnServerRefreshClicked(object sender, EventArgs e)
	{
		if(rbServerInternet.Active)
		{
			RefreshInternetServers();
		}
		else if(rbServerFavorites.Active)
		{
			RefreshFavoriteServers();
		}
		else if(rbServerHistory.Active)
		{
			RefreshHistoryServers();
		}
		else if(rbServerLan.Active)
		{
			RefreshLANServers();
		}
		else if(rbServerFriends.Active)
		{
			RefreshFriendsServers();
		}
		// TODO remaining query types as we add them in
	}

	protected void OnBtnServerConnectDirectClicked(object sender, EventArgs e)
	{
		// TODO a prompt of some kind for inputting a specific server IP
		// Get its info and rules
	}

	private void UpdateServerConnectDetailsButtons()
	{
        btnAddServerToFavorites.Sensitive = SelectedServer != null;

		btnSelectedServerConnect.Sensitive = SelectedServer != null;
		btnSelectedServerDetails.Sensitive = SelectedServer != null;
	}

	private void OpenDetailsWindow(Server server, bool startWithDetailsOpen = false)
	{
		BA3L.GTK2UI.ServerInfoWindow serverInfoWindow = 
			new BA3L.GTK2UI.ServerInfoWindow(core, server, startWithDetailsOpen);
		serverInfoWindow.WindowPosition = WindowPosition.Center;
		serverInfoWindow.Show();
	}

	protected void OnBtnSelectedServerDetailsClicked(object sender, EventArgs e)
	{
		if(SelectedServer != null)
		{
			OpenDetailsWindow(SelectedServer, true);
		}
	}

	protected void OnBtnSelectedServerConnectClicked(object sender, EventArgs e)
	{
		if(SelectedServer != null)
		{
			OpenDetailsWindow(SelectedServer, false); 
		}
	}

	protected void OnRbFilterBattlEyeAnyToggled(object sender, EventArgs e)
	{
		if(!rbFilterBattlEyeAny.Active)
			return;

		ServerFilters.BattlEye = BattlEyeFilter.Any;
		tvServerListFilter.Refilter();
	}

	protected void OnRbFilterBattlEyeYesToggled(object sender, EventArgs e)
	{
		if(!rbFilterBattlEyeYes.Active)
			return;

		ServerFilters.BattlEye = BattlEyeFilter.Protected;
		tvServerListFilter.Refilter();
	}

	protected void OnRbFilterBattlEyeNoToggled(object sender, EventArgs e)
	{
		if(!rbFilterBattlEyeNo.Active)
			return;

		ServerFilters.BattlEye = BattlEyeFilter.NotProtected;
		tvServerListFilter.Refilter();
	}

	protected void OnBtnFilterNoPasswordToggled(object sender, EventArgs e)
	{
		ServerFilters.NoPasswordRequired = btnFilterNoPassword.Active;
		tvServerListFilter.Refilter();
	}

	protected void OnBtnFilterNotFullToggled(object sender, EventArgs e)
	{
		ServerFilters.NotFull = btnFilterNotFull.Active;
		tvServerListFilter.Refilter();
	}

	protected void OnBtnFilterHasPlayersToggled(object sender, EventArgs e)
	{
		ServerFilters.HasPlayers = btnFilterHasPlayers.Active;
		tvServerListFilter.Refilter();
	}

    protected void OnBtnFilterSameVersionToggled(object sender, EventArgs e)
    {
        ServerFilters.SameVersion = btnFilterSameVersion.Active;
        tvServerListFilter.Refilter();
    }

    protected void OnBtnAddServerToFavoritesClicked(object sender, EventArgs e)
    {
        if(SelectedServer == null)
            return;

        if(rbServerFavorites.Active)
        {
            // We're on the favorites page, so remove the selected server from favs
            // For some reason I can't see a way to check if a server is already favorited so we do it like this.
            SelectedServer.RemoveFromFavorites();
        }
        else
        {
            // Not on the favorites page, add to favorites
            SelectedServer.AddToFavorites();
        }
    }

    protected void OnTeFilterServerNameChanged(object sender, EventArgs e)
    {
        ServerFilters.ServerName = teFilterServerName.Text;
        tvServerListFilter.Refilter();
    }



    #endregion
}
