﻿using System.Collections.Generic;
namespace BA3L
{
	public struct ModPresetStruct
	{
		public List<string> LocalModIdentifiers;
		public List<ulong>  WorkshopModIds;
	}
}
