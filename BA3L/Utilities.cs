﻿using System;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using Steamworks;
using System.Collections.Generic;
using Gameloop.Vdf;
using Gameloop.Vdf.Linq;

namespace BA3L
{
	public static class Utilities
	{
		public const int ARMA3_APPID = 107410;

        // Only find the path the first time, then cache it.
        private static string _steamPath = "";
        public static string SteamPath
        {
            get
            {
                if(string.IsNullOrEmpty(_steamPath))
                {
                    _steamPath = GetSteamPath();
                }

                return _steamPath;
            }
        }

        // Only find the path the first time, then cache it.
        private static string _arma3Path = "";
        public static string Arma3Path
        {
            get
            {
                if(string.IsNullOrEmpty(_arma3Path))
                {
                    _arma3Path = GetArma3Path();
                }

                return _arma3Path;
            }
        }

        private static string _gameVersion = "";
        public static string GameVersion
        {
            get
            {
                if(string.IsNullOrEmpty(_gameVersion))
                {
                    _gameVersion = GetGameVersion();
                }

                return _gameVersion;
            }
        }

        public static string ArgsToString(string[] args)
		{
			string argsString = "";

			foreach (string arg in args)
			{
				argsString += arg + " ";
			}

			return argsString;
		}

		public enum OS
		{
			OS_UNKNOWN,
			OS_WINDOWS,
			OS_OSX,
			OS_LINUX
		};

		public static OS GetOS()
		{
			// Passing this here in case I decide to not use IsOSPlatform
			// Which only exists in .NET 4.7.1 (October 2017) or newer
			// I probably will keep using IsOSPlatform so this may need refactoring.
			if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
			{
				return OS.OS_WINDOWS;
			}
			else if(RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
			{
				return OS.OS_OSX;
			}
			else if(RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
			{
				return OS.OS_LINUX;
			}
			else
			{
				return OS.OS_UNKNOWN;
			}
		}

        private static string GetSteamPath()
        {
            // Search paths
            string homeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string[] searchPaths =
            {
                // Installed via package manager
                $"{homeDir}/.local/share/Steam/",
                $"{homeDir}/.steam/root/",
                // Flatpak
                $"{homeDir}/.var/app/com.valvesoftware.Steam/.steam/root/",
                $"{homeDir}/.var/app/com.valvesoftware.Steam/.local/share/Steam/",
                // macOS
                $"{homeDir}/Library/Application Support/Steam/"
            };

            
            foreach(string searchPath in searchPaths)
            {
                if(Directory.Exists(searchPath))
                {
                    // Could potentially return the wrong path if you installed via package manager
                    // and then switched to Flatpak
                    // so maybe delete the unnecessary folder if you did that.
                    return searchPath;
                }
            }

            // If we get to here, we didn't find a Steam path
            throw new DirectoryNotFoundException("Unable to find Steam installation directory!");
        }

        private static string GetArma3Path()
		{
			if(SteamClient.IsValid)
			{
				// TODO consider doing something if this is null (e.g. game uninstalled)
				return SteamApps.AppInstallDir(ARMA3_APPID);
			}
			else
			{
				// We can't really assume it's installed in the default location
				// If Windows:
				if (GetOS() == OS.OS_WINDOWS)
				{
					// Windows can use a registry entry
					string registryPath;
					if (Environment.Is64BitOperatingSystem)
					{
						registryPath = "HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\bohemia interactive\\arma 3";
					}
					else
					{
						registryPath = "HKEY_LOCAL_MACHINE\\Software\\bohemia interactive\\arma 3";
					}

					string arma3Path = Microsoft.Win32.Registry.GetValue(registryPath, "main", "").ToString();
					if (arma3Path != "")
					{
						return arma3Path;
					}
					else
					{
						throw new DirectoryNotFoundException("Unable to find Arma 3 path. Ensure it has been run once.");
					}
				}
				else if (GetOS() == OS.OS_LINUX)
				{
                    // Search steamapps directories for Arma 3
                    // The manifest file will exist in the appropriate directory: appmanifest_107410.acf
                    // Other steamapps can be found in ~/.local/share/Steam/config/config.vdf
                    // For now assume the default SteamApps location is used

                    string arma3Path = Path.Combine(SteamPath, "steamapps/common/Arma 3/");
					if (Directory.Exists(arma3Path))
					{
						return arma3Path;
					}
					else
					{
						throw new DirectoryNotFoundException("Unable to manually locate Arma 3 path. TODO scan other steamapps folders. Or you can (re)start Steam.");
					}
				}
				else if (GetOS() == OS.OS_OSX)
				{
					// MacOS not yet supported
					throw new Exceptions.UnsupportedOperatingSystemException("MacOS not supported yet");
				}
				else
				{
					throw new Exceptions.UnsupportedOperatingSystemException("Unknown Operating System!");
				}
			}
		}

		/// <summary>
		/// Determines whether the install of Arma 3 is using Proton.
		/// (If arma3launcher.exe exists and the host OS is Linux, we say it is Proton.)
		/// </summary>
		/// <returns><c>true</c>, if the install uses Proton, <c>false</c> otherwise.</returns>
		public static bool IsProton()
		{
			// This is another method that could have been a property.
			string launcherPath = Path.Combine(Arma3Path, "arma3launcher.exe");
			return File.Exists(launcherPath) && (GetOS() == OS.OS_LINUX);
		}

		private static string GetGameVersion()
		{
			if(GetOS() == OS.OS_WINDOWS || IsProton())
			{
				string gamePath = Arma3Path;
				string exeName = "arma3.exe";
				string gameexePath = Path.Combine(gamePath, exeName);
				FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(gameexePath);
                return versionInfo.FileVersion;
			}
			else
			{
				return "1.82"; // TODO find out if there is version information on the native version
			}
		}

        /// <summary>
        /// Gets the proton version. Assumes this is only called from a Proton-compatible OS.
        /// </summary>
        /// <returns>a KeyValuePair containing the Proton version and its AppID (0 if it has no AppID).</returns>
        public static KeyValuePair<string, int> GetProtonVersion()
        {
            var configVdfPath = Path.Combine(SteamPath, "config", "config.vdf");
            string configVdfText = File.ReadAllText(configVdfPath);
            VProperty configVdf = VdfConvert.Deserialize(configVdfText, 
                new VdfSerializerSettings() { MaximumTokenSize = 8192, UsesEscapeSequences = true });

            var protonVersionString = configVdf.Value["Software"]["Valve"]["Steam"]["CompatToolMapping"][ARMA3_APPID.ToString()]["name"].ToString();

            // Some of the Proton versions are Steam apps and therefore have app IDs
            Console.WriteLine("Proton version string: " + protonVersionString);
            Dictionary<string, int> protonVersionAppIds = new Dictionary<string, int>
            {
                {"proton_37", 858280},
                {"proton_316", 961940},
                {"proton_42", 1054830},
                {"proton_411", 1113280},
                {"proton_5", 1245040},
                {"proton_513", 1420170},
                {"proton_63", 1580130},
                {"proton_7", 1887720},
                {"proton_experimental", 1493710},
            };
            // Future Proton versions wil need to be added later.
            // But the user might be using a custom Proton version that doesn't have an App ID

            if(protonVersionAppIds.ContainsKey(protonVersionString))
            {
                return new KeyValuePair<string, int>(protonVersionString, protonVersionAppIds[protonVersionString]);
            }
            else
            {
                return new KeyValuePair<string, int>(protonVersionString, 0);
            }
        }

        public static string GetProtonPath()
        {
            var protonVersion = GetProtonVersion();

            if(protonVersion.Value != 0)
            {
                // If it's an official Proton version then just find its path from its AppId
                return Path.Combine(SteamApps.AppInstallDir(protonVersion.Value), "proton");
            }
            else
            {
                // TODO the name is sometimes different from the folder
                // To do this properly we'd need to check all the manifests in compatibiliytools.d to find the right name
                // But for now this'll probably do.
                Console.WriteLine($"Custom Proton version: {protonVersion.Key}");
                return Path.Combine(SteamPath, "compatibilitytools.d", protonVersion.Key, "proton");
            }
        }
    }
}
