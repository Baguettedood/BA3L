﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Steamworks;
using System.Text;

namespace BA3L
{
	public enum RefreshModsReason
	{
		None,
		WorkshopModsReady,
		SubscribedToMods
	};

	public class RefreshModsEventArgs : EventArgs
	{
		public RefreshModsReason Reason;

		public RefreshModsEventArgs(RefreshModsReason reason)
		{
			Reason = reason;
		}
	}

    // Called when mods are toggled, or after a mod list is loaded.
    // TODO consider if we want to include the list of changed mods
    public class EnabledModsChangedEventArgs : EventArgs {} 

    // TODO make this static at some point
    public static class ModManager
	{
		private static List<Mod> mods;
		private static List<Mod> enabledMods;
		private static readonly object _modLock = new object();

		public static bool workshopModsReady { get; private set; }
		private static bool loadWorkshopModsWhenPossible = false;
		public static event EventHandler<RefreshModsEventArgs> RefreshModsEvent;
        public static event EventHandler<EnabledModsChangedEventArgs> EnabledModsChangedEvent;

		// If any mods are not ready to play, we clearly aren't ready.
		public static bool EnabledModsReady
		{
			get
			{
				foreach(Mod enabledMod in enabledMods)
				{
					if(!enabledMod.IsReadyToPlay)
						return false;
				}

				return true;
			}
		}

		public static void Init()
		{
			enabledMods = new List<Mod>();
			DiscoverMods();
		}

		public static ReadOnlyCollection<Mod> GetMods()
		{
			return new ReadOnlyCollection<Mod>(mods);
		}

		public static ReadOnlyCollection<Mod> GetEnabledMods()
		{
			return new ReadOnlyCollection<Mod>(enabledMods);
		}

		public static void LoadSavedMods()
		{
			LoadSavedLocalMods();

			if(workshopModsReady)
			{
				LoadSavedWorkshopMods();
			}
			else
			{
				// Do it soon
				lock(_modLock)
				{
					loadWorkshopModsWhenPossible = true;
				}
			}
		}

		private static void LoadSavedLocalMods()
		{
			if(Settings.SavedLocalMods == null)
				return;

			foreach(string path in Settings.SavedLocalMods)
			{
				EnableModByPath(path);
			}

            OnEnabledModsChanged();
		}

		private static void LoadSavedWorkshopMods()
		{
			if(Settings.SavedWorkshopMods == null)
				return;

			foreach(ulong workshopId in Settings.SavedWorkshopMods)
			{
				EnableModByWorkshopId(workshopId);
			}

            OnEnabledModsChanged();
		}

		public static void LoadModPreset(ModPresetStruct modPreset)
		{
			DisableAllMods();


			// Local mods:
			foreach(string modIdentifier in modPreset.LocalModIdentifiers)
			{
				EnableModByFolderName(modIdentifier);
			}

			// Workshop mods:
			List<ulong> missingWorkshopMods = new List<ulong>();
			if(workshopModsReady) // Throw if it isn't ready?
			{
				foreach(ulong workshopModId in modPreset.WorkshopModIds)
				{
					if(!EnableModByWorkshopId(workshopModId))
					{
						// If the mod isn't enabled
						missingWorkshopMods.Add(workshopModId);
					}
				}
			}

			if(missingWorkshopMods.Any())
			{
				// Apparently this is how you pass arguments to async methods
				Task.Run(() => DownloadWorkshopMods(missingWorkshopMods, true));
			}

            OnEnabledModsChanged();
		}

		public static async Task DownloadWorkshopMod(ulong missingWorkshopMod, bool enableSubscribedMod = false)
		{
			List<ulong> oneModList = new List<ulong>();
			oneModList.Add(missingWorkshopMod);
			await DownloadWorkshopMods(oneModList, enableSubscribedMod);
		}

		private static async Task DownloadWorkshopMods(List<ulong> missingWorkshopMods, bool enableSubscribedMods = false)
		{
			List<Mod> newMods = new List<Mod>();
			foreach(ulong modId in missingWorkshopMods)
			{
				var itemNullable = await Steamworks.Ugc.Item.GetAsync(modId);
				var item = itemNullable.GetValueOrDefault();
				if(!item.Equals(default(Steamworks.Ugc.Item)) 
					&& item.ConsumerApp == Utilities.ARMA3_APPID // Sanity check: Don't subscribe to items that don't belong to Arma 3.
					&& item.HasTag("Mod")) 
				{
					// Don't await item.Subscribe() since it never seems to finish waiting?
					// Either that or I mistook an issue earlier.
					var subscribeTask = Task.Run(item.Subscribe).ConfigureAwait(false);
					Mod newMod = new Mod(item);
					lock(_modLock)
					{
						mods.Add(newMod);
						if(enableSubscribedMods)
						{
							EnableMod(newMod);
						}
					}
				}
			}

			OnRefreshMods(RefreshModsReason.SubscribedToMods);
		}

		private static void OnWorkshopModsReady()
		{
			workshopModsReady = true;

			lock(_modLock)
			{
				if(loadWorkshopModsWhenPossible)
				{
					LoadSavedWorkshopMods();
				}
			}

			OnRefreshMods(RefreshModsReason.WorkshopModsReady);
		}

		private static void OnRefreshMods(RefreshModsReason reason)
		{
			RefreshModsEventArgs args = new RefreshModsEventArgs(reason);
			RefreshModsEvent?.Invoke(null, args);
		}

        private static void OnEnabledModsChanged()
        {
            EnabledModsChangedEventArgs args = new EnabledModsChangedEventArgs();
            EnabledModsChangedEvent?.Invoke(null, args);
        }

        private static void DiscoverMods()
		{
			List<Mod> discoveringMods = new List<Mod>();
			discoveringMods.AddRange(FindLocalMods());
			lock(_modLock)
			{
				mods = discoveringMods;
			}

			var task = Task.Run(FindWorkshopMods).ConfigureAwait(false);
		}

		public static async Task FindWorkshopMods()
		{
			if (!SteamClient.IsValid)
				return;

			List<Mod> workshopMods = new List<Mod>();

			var query = Steamworks.Ugc.Query.ItemsReadyToUse.WhereUserSubscribed().WithTag("Mod");

			bool pageHasEntries = true;
			int page = 1;
			while(pageHasEntries)
			{
				var result = await query.GetPageAsync(page);
				var entries = result.Value.Entries;

				if(entries.Any())
				{
					foreach (Steamworks.Ugc.Item item in result.Value.Entries)
					{
						if(item.HasTag("Mod")) // Can't use IsMod(directory) since it might be downloading
						{
                            try
                            {
                                Mod NewMod = new Mod(item);
                                workshopMods.Add(NewMod);
                            }
                            catch(Exception e)
                            {
                                Console.Error.WriteLine(e);
                            }
                        }
					}

					page++;
				}
				else
				{
					pageHasEntries = false;
				}
			}

            Console.WriteLine($"Finished processing mods");


            lock (_modLock)
			{
				mods.AddRange(workshopMods);
			}

			OnWorkshopModsReady();
		}

		/// <summary>
		/// Looks for mods in the specified directory (currently Arma 3's install dir)
		/// </summary>
		/// <returns>Mods that were found.</returns>
		public static List<Mod> FindLocalMods()
		{
			List<Mod> localMods = new List<Mod>();

			string arma3Dir = Utilities.Arma3Path;

			// Look through all folders in Arma 3 Directory to find mods
			var directories = Directory.EnumerateDirectories(arma3Dir);

            // TODO include additional directories

            string[] ignoredFolderNames = {
                "Addons",
				"AoW",
				"Argo",
				"BattlEye",
				"Bonus",
				"Contact",
                "CSLA",
				"Curator",
				"Dll",
                "Dta",
				"Enoch",
				"Expansion",
				"GM",
				"Heli",
				"Jets",
				"Kart",
				"Keys",
                "launcher",
				"Mark",
				"Missions",
				"MPMissions",
				"Orange",
				"Tacops",
				"Tank",
                "vn",
                "WS"
			}; 
			// TODO Official launcher has DLC tab so handle DLCs elsewhere.

			foreach(string directory in directories)
			{
				string lastFolderName = new DirectoryInfo(@directory.TrimEnd(Path.DirectorySeparatorChar)).Name;
				if(ignoredFolderNames.Contains(lastFolderName))
					continue;

				if (IsMod(directory))
				{
					// Generate mod info
					Mod newMod = new Mod(directory);
					localMods.Add(newMod);
				}
			}

			return localMods;
		}

		static bool IsMod(string fullModFolderPath)
		{
			return ContainsModCpp(fullModFolderPath) || ContainsAddonsFolder(fullModFolderPath);
		}

		static bool ContainsModCpp(string fullModFolderPath)
		{
			// Most mods contain this, some mods do not such as compat mods?
			string modcpp = Path.Combine(fullModFolderPath, "mod.cpp");
			return File.Exists(modcpp);
		}

		static bool ContainsAddonsFolder(string fullModFolderPath)
		{
			// Case sensitivity
			var directories = Directory.EnumerateDirectories(fullModFolderPath);
			foreach(string directory in directories)
			{
				if(directory.ToLower().EndsWith("addons", StringComparison.Ordinal))
				{
					return true;
				}
			}

			return false;
		}

		public static bool FindModByWorkshopId(ulong workshopId, out Mod foundMod)
		{
			if(workshopId == 0uL)
			{
				foundMod = null;
				return false;
			}

			foundMod = mods.FirstOrDefault(x => x.WorkshopId == workshopId);
			if(foundMod == default(Mod))
				return false;
			else
				return true;
		}

		public static List<Mod> FindModsWithSignature(string signature)
		{
			// Unless we want sorting on this, we should be fine.
			return mods.FindAll(x => x.HasSignature(signature) == true);
		}

		public static List<Mod> FindModsByName(string name)
		{
			return mods.FindAll(x => x.Name.Equals(name));
		}

		public static bool EnableModByFolderName(string localIdentifier) // Local
		{
			// FirstOrDefault or SingleOrDefault?
			Mod modToEnable = mods.FirstOrDefault(x => x.ModFolderName == localIdentifier);
			return EnableMod(modToEnable);
		}

		public static bool EnableModByPath(string path)
		{
			Mod modToEnable = mods.FirstOrDefault(x => x.GetPath() == path);
			return EnableMod(modToEnable);
		}

		public static bool EnableModByWorkshopId(ulong workshopIdentifier) // Workshop
		{
			if(!workshopModsReady)
				return false;

			Mod modToEnable = mods.FirstOrDefault(x => x.WorkshopId == workshopIdentifier);
			return EnableMod(modToEnable);
		}

		public static bool EnableMod(Mod modToEnable, bool announceChange = true)
		{
			if(modToEnable == null)
				return false;

			if(enabledMods.Contains(modToEnable))
				return true;

			modToEnable.Enable();
			enabledMods.Add(modToEnable);
			return true;
		}

		public static bool DisableModByFolderName(string localIdentifier)
		{
			Mod modToDisable = enabledMods.FirstOrDefault(x => x.ModFolderName == localIdentifier);
			return DisableMod(modToDisable);
		}

		public static bool DisableModByWorkshopId(ulong workshopIdentifier)
		{
			if(!workshopModsReady)
				return false;

			Mod modToDisable = enabledMods.FirstOrDefault(x => x.WorkshopId == workshopIdentifier);
			return DisableMod(modToDisable);
		}

		public static bool DisableMod(Mod modToDisable)
		{
			if(modToDisable == null)
				return false;

			modToDisable.Disable();
			enabledMods.Remove(modToDisable);
			return true;
		}

		public static void DisableAllMods()
		{
			// Avoid iterating on a list I'm manipulating.
			List<Mod> modsToDelete = new List<Mod>(mods);
			lock(_modLock)
			{
				foreach(Mod mod in modsToDelete)
				{
					DisableMod(mod);
				}
			}
		}

		/// <summary>
		/// Toggles the given mod
		/// </summary>
		/// <returns><c>true</c>, if the mod is now enabled, <c>false</c> if the mod is now disabled.</returns>
		/// <param name="modToToggle">Mod to toggle.</param>
		public static bool ToggleMod(Mod modToToggle)
		{
			if(modToToggle.Enabled)
			{
				DisableMod(modToToggle);
				return false;
			}
			else
			{
				EnableMod(modToToggle);
				return true;
			}
		}

		public static string GenerateModLaunchParameters()
		{
			// Windows:
			// -mod=test;x\test;c:\arma3\test2

			// Linux:
			// -mod=mod1\;mod2\;mod3
			// -mod=test\;x/test\;/home/user/arma3/test2

			StringBuilder sb = new StringBuilder();

			sb.Append("-mod=\"");

			foreach(Mod mod in enabledMods)
			{
				if(!mod.IsReadyToPlay)
				{
					throw new Exceptions.ModNotReadyException(mod);
				}

				string path;
				if(Utilities.IsProton())
				{
					path = mod.GetProtonPath();
				}
				else
				{
					path = mod.GetPath();
				}

				sb.Append(path);

				if(Utilities.GetOS() == Utilities.OS.OS_WINDOWS || Utilities.IsProton())
				{
					// Append ;
					sb.Append(";");
				}
				else
				{
					// Append \;
					sb.Append(@"\;");
				}
			}

			sb.Append("\"");

			return sb.ToString();
		}
	}
}
