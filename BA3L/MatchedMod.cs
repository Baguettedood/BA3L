﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BA3L
{

	public enum ModMatchType
	{
		Unknown,
		Modlist,
		Signature,
		NotAllowed,
	}

	public class MatchedMod
	{
		public ServerMod ServerMod;
		public string MatchedModName { get; private set; } // Can't guarantee ServerMod isn't null so we put the name here
		public bool WasMatched;
		public List<ModActions.MatchedModAction> Actions { get; private set; }
		public ModActions.MatchedModAction CurrentAction;

		public ModMatchType MatchType = ModMatchType.Unknown;

		public MatchedMod(string matchedName)
		{
			Actions = new List<ModActions.MatchedModAction>();
			MatchedModName = matchedName;
		}

		public void AddAction(ModActions.MatchedModAction ActionToAdd)
		{
			Actions.Add(ActionToAdd);

			// Assume the default action should be the first action
			if(CurrentAction == null)
			{
				CurrentAction = ActionToAdd;
			}
		}

		public void SetCurrentActionByString(string actionString)
		{
			// Find first action whose ToString() matches our parameter.
			ModActions.MatchedModAction actionToSet = Actions.FirstOrDefault(action => action.ToString() == actionString);
			if(actionToSet != null)
			{
				CurrentAction = actionToSet;
			}
		}
	}

}
