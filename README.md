Baguettedood's Arma 3 Launcher
==============================

An Arma 3 Launcher that can work as a standalone launcher, or as a replacement for Bohemia's official Arma 3 Launcher. Primarily intended for Proton.

[![pipeline status](https://gitlab.com/Baguettedood/BA3L/badges/master/pipeline.svg)](https://gitlab.com/Baguettedood/BA3L/commits/master)


### Features

* Launch the game
* Support for mods in Arma 3 directory
* Toggling Steam Workshop mods
* Saving/loading mod presets
* Subscribing to mods if needed
* Limited number of launch parameters
* Server browser with mod detection (GTK only)


### TODO

* More server browser options
* More parameters
* Detecting potential issues with prefix/missing libraries (e.g. libpng needed for thermals)
* Automatic Proton version detection
* User-defined environment variables?


### Building

#### Requirements

Development versions of these tools required if your distro keeps those versions separate.

* mono
* nuget
* gtk-sharp-2
* MSBuild or MonoDevelop (mdtools), or another build system that works.
* More requirements TBD as I figure them out.

#### Build process

    git clone https://gitlab.com/Baguettedood/BA3L
    cd BA3L
    ./Build/build_mdtool_release.sh or ./Build/build_msbuild_release.sh

### Running

    cd Build/Release/BA3L
    mono BA3L.exe

Alternatively, you can use BA3L in place of Arma 3's official Windows launcher.

* Backup arma3launcher.exe in Arma 3's directory
* Copy the contents of Build/Release (or your preferred configuration) into Arma 3's directory.
* The structure should have BA3L's arma3launcher.exe in the Arma 3 directory, and a BA3L folder also inside.
* Remove `-nolauncher` from Arma 3's launch options
* Run Arma 3 through Steam